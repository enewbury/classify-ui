import {FILTER_SEARCH} from "./actionTypes";

export const filterResults = (filters) => {
    return {
        type: FILTER_SEARCH,
        filters: filters
    }
};