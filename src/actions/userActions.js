
import {
    ATTEMPT_REGISTER, ATTEMPTING_LOGIN, EMAIL_UPDATED, LOGIN_FAILED,
    LOGIN_SUCCESSFUL, LOGOUT,
    NAME_UPDATED,
    REGISTER_FAILED,
    REGISTER_SUCCESSFUL
} from "./actionTypes";
import {API, SERVER} from "../helper/constants";
import {restCall} from "../helper/apiHelpers";

export const attemptLogin = (formData) => {
    return (dispatch) => {
        dispatch({type: ATTEMPTING_LOGIN});
        fetch(SERVER + API + "/session", {
            method: "POST",
            credentials: 'include',
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            body: formData
        }).then(
            response => {
                if (response.ok) {
                    response.json().then(json => {
                        dispatch({type: LOGIN_SUCCESSFUL, user:json}
                    )});
                } else {
                    response.json().then(json => {
                        dispatch({type: LOGIN_FAILED, message: json.message, exception: json.exception});
                    });
                }
            },
            errorResponse => {
                dispatch({type: LOGIN_FAILED, message: "Unable to connect to server at this time."});
            }
        );
    }
};

export const attemptRegistration = (email, password, confirm) => {
    return (dispatch) => {
        dispatch({type: ATTEMPT_REGISTER});

        fetch(SERVER + API + "/user", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({email, password})
        }).then(
            response => {
                if (response.ok) {
                    dispatch({type: REGISTER_SUCCESSFUL});
                } else {
                    response.json().then(json => {
                        dispatch({type: REGISTER_FAILED, message: json.message, exception: json.exception});
                    });
                }
            },
            errorResponse => {
                dispatch({type: REGISTER_FAILED, message: "Unable to connect to server at this time."});
            }
        );
    };
};

export const resendVerificationEmail = (email) => {
    return (dispatch) => {
        restCall("/user/verification-token", {method: "POST", body: JSON.stringify({email})}, json => {}, dispatch);
    }
};

export const logout = () => {
    return (dispatch) => {
        restCall("/session", {method: "DELETE"}, json => {
            dispatch({type: LOGOUT});
        }, dispatch);
    };
};

export const updateName = (firstName, lastName) => {
    return (dispatch) => {
        restCall("/user/me?type=NAME", {method: "PATCH", body: JSON.stringify({firstName, lastName})}, json => {}, dispatch);
        dispatch({
            type: NAME_UPDATED,
            firstName, lastName
        })
    }
};

export const updateEmail = (email) => {
    return {
        type: EMAIL_UPDATED,
        email
    }
};

export const updatePassword = (password) => {
    return (dispatch) => {
        restCall("/user/me?type=PASS", {method: "PATCH", body: JSON.stringify({password})}, json => {}, dispatch);
    }
};