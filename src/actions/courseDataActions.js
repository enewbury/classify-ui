import { normalize } from "normalizr";
import { showLoading, hideLoading } from "react-redux-loading-bar";
import {
    FETCH_TERMS_REQUESTED, FETCH_TERMS_RECEIVED, SELECT_TERM, FETCH_COURSE_DATA_REQUESTED, FETCH_COURSE_DATA_RECEIVED,
    SECTION_METRICS_RECEIVED
} from "./actionTypes";
import { fetchSchedules } from "./listsActions";

import * as schema from "./schema"
import {restCall} from "../helper/apiHelpers";
import {fetchWatchList} from "./watchListActions";

export const fetchTerms = (callback = () => {}) => {
    return (dispatch) => {
        dispatch({type: FETCH_TERMS_REQUESTED});

        restCall("/course-data/terms", {}, (json) => {
            dispatch({
                type: FETCH_TERMS_RECEIVED,
                terms: json
            });
            callback();
        }, dispatch);
    }
};

export const selectTerm = (termId) => {
    return (dispatch) => {

        //set the selected term
        dispatch({
            type: SELECT_TERM,
            selectedTermId: termId
        });

        //save to local storage
        localStorage.setItem("selectedTermId", termId);

        //load course data
        dispatch({type: FETCH_COURSE_DATA_REQUESTED, termId: termId});

        //load user lists
        dispatch(fetchSchedules(termId));

        //load watch list
        dispatch(fetchWatchList(termId));

        //show loading bar
        dispatch(showLoading());

        restCall("/course-data/term/"+termId, {}, json => {
                dispatch({
                    type: FETCH_COURSE_DATA_RECEIVED,
                    termId: termId,
                    courseData: normalize(json, [schema.course])
                });
                dispatch(hideLoading());
        }, dispatch);
    }
};

export const fetchSectionChanges = (sectionId) => {
    return (dispatch) => {
        restCall("/course-data/section/"+sectionId+"/changes", {}, json => {
            dispatch({
                type: SECTION_METRICS_RECEIVED,
                sectionId: sectionId,
                changes: json.changes
            })
        }, dispatch);
    }
};

