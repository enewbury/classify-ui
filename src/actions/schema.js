import { schema } from "normalizr";


export const instructor = new schema.Entity("instructors");
export const section = new schema.Entity('sections', {
    instructor: instructor
});
export const course = new schema.Entity("courses", {
    sections: [section]
});


export const listEntry = new schema.Entity('entries');
export const entries = new schema.Array(listEntry);
listEntry.define({ entries });