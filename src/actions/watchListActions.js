import {restCall} from "../helper/apiHelpers";
import {ADD_TO_WATCHLIST, REMOVE_FROM_WATCHLIST_BY_SECTION, WATCH_LIST_ENTRIES_RECEIVED} from "./actionTypes";

export const fetchWatchList = (termId) => {
    return (dispatch) => {
        restCall("/watchList?termId="+termId, {}, json => {
            dispatch({
               type: WATCH_LIST_ENTRIES_RECEIVED,
               entries: json
            });
        }, dispatch);
    }
};

export const addToWatchList = (termId, sectionId) => {
    return (dispatch) => {
        restCall("/watchList?termId="+termId, {method: "POST", body: JSON.stringify({sectionId})}, json => {
            dispatch({
                type: ADD_TO_WATCHLIST,
                entry: json
            })
        }, dispatch)
    }
};

export const removeFromWatchListBySection = (termId, sectionId) => {
    return (dispatch) => {
        restCall("/watchList?termId="+termId+"&sectionId="+sectionId, {method: "DELETE"}, json => {
            dispatch({type: REMOVE_FROM_WATCHLIST_BY_SECTION, sectionId: sectionId});
        }, dispatch);
    }
};