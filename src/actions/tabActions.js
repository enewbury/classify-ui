import {CLOSE_SECTION_TAB, SELECT_SECTION_TAB} from "./actionTypes";


export const selectTab = (index, termId) => {
    return {
        type: SELECT_SECTION_TAB,
        index: index,
        termId: termId
    };

};

export const closeTab = (index, termId) => {
    return {
        type: CLOSE_SECTION_TAB,
        index: index,
        termId: termId
    }
};