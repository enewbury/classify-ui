export const ADD_ERROR = "ADD_ERROR";
export const REMOVE_ERROR = "REMOVE_ERROR";

export const ATTEMPTING_LOGIN = "ATTEMPTING_LOGIN";
export const LOGIN_SUCCESSFUL = "LOGIN_SUCCESSFUL";
export const LOGIN_FAILED = "LOGIN_FAILED";
export const LOGOUT = "LOGOUT";

export const ATTEMPT_REGISTER = "ATTEMPT_REGISTER";
export const REGISTER_SUCCESSFUL = "REGISTER_SUCCESSFUL";
export const REGISTER_FAILED = "REGISTER_FAILED";

export const FETCH_TERMS_REQUESTED = "FETCH_TERMS_REQUESTED";
export const FETCH_TERMS_RECEIVED = "FETCH_TERMS_RECEIVED";

export const SELECT_TERM = "SELECT_TERM";
export const FETCH_COURSE_DATA_REQUESTED = "FETCH_COURSE_DATA_REQUESTED";
export const FETCH_COURSE_DATA_RECEIVED = "FETCH_COURSE_DATA_RECEIVED";
export const SECTION_METRICS_RECEIVED = "SECTION_METRICS_RECEIVED";

export const FETCH_SCHEDULES_REQUESTED = "FETCH_SCHEDULES_REQUESTED";
export const FETCH_SCHEDULES_RECEIVED = "FETCH_SCHEDULES_RECEIVED";
export const SELECT_LIST_ENTRY = "SELECT_LIST_ENTRY";
export const VIEW_SECTION = "VIEW_SECTION";
export const SELECT_SECTION_TAB = "SELECT_SECTION_TAB";
export const CLOSE_SECTION_TAB = "CLOSE_SECTION_TAB";


export const ADD_SECTION_TO_SCHEDULE_SUCCESSFUL = "ADD_SECTION_TO_SCHEDULE_SUCCESSFUL";
export const MOVE_ENTRY = "MOVE_ENTRY";

export const ADD_SCHEDULE_SUCCESSFUL = "ADD_SCHEDULE_SUCCESSFUL";

export const ADD_FOLDER_SUCCESSFUL = "ADD_FOLDER_SUCCESSFUL";

export const UPDATE_SCHEDULE_NAME_SUCCESSFUL = "UPDATE_SCHEDULE_NAME_SUCCESSFUL";

export const UPDATE_GROUP_NAME_SUCCESSFUL = "UPDATE_GROUP_NAME_SUCCESSFUL";

export const DELETE_LIST_ENTRY_SUCCESSFUL = "DELETE_LIST_ENTRY_SUCCESSFUL";

export const WATCH_LIST_ENTRIES_RECEIVED = "WATCH_LIST_ENTRIES_RECEIVED";
export const ADD_TO_WATCHLIST = "ADD_TO_WATCHLIST";
export const REMOVE_FROM_WATCHLIST_BY_SECTION = "REMOVE_FROM_WATCHLIST_BY_SECTION";

export const FILTER_SEARCH = "FILTER_SEARCH";

export const NAME_UPDATED = "NAME_UPDATED";
export const EMAIL_UPDATED = "EMAIL_UPDATED";

export const SHOW_ADD_ENTRY_MODAL = "SHOW_ADD_ENTRY_MODAL";
export const SHOW_MOVE_ENTRY_MODAL = "SHOW_MOVE_ENTRY_MODAL";
export const HIDE_ADD_MOVE_MODAL = "HIDE_ADD_MOVE_MODAL";
