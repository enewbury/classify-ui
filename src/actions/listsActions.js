import {
    FETCH_SCHEDULES_REQUESTED, FETCH_SCHEDULES_RECEIVED,
    VIEW_SECTION, SELECT_LIST_ENTRY, ADD_SECTION_TO_SCHEDULE_SUCCESSFUL,
    ADD_SCHEDULE_SUCCESSFUL, ADD_FOLDER_SUCCESSFUL,
    UPDATE_SCHEDULE_NAME_SUCCESSFUL, UPDATE_GROUP_NAME_SUCCESSFUL, DELETE_LIST_ENTRY_SUCCESSFUL, MOVE_ENTRY
} from "./actionTypes";
import * as schema from "./schema";
import {normalize} from "normalizr";
import {restCall} from "../helper/apiHelpers";


export const fetchSchedules = (termId) => {
    return (dispatch) => {
        dispatch({type: FETCH_SCHEDULES_REQUESTED});
        restCall("/lists?termId="+termId, {}, json => {
            dispatch({
                type: FETCH_SCHEDULES_RECEIVED,
                entries: normalize(json, schema.entries).entities.entries,
                termId: termId
            });
        }, dispatch);
    }
};

export const selectEntry = (entryId) => {

    return (dispatch, getState) => {
        let {selectedTermId} = getState();
        let entries = JSON.parse(localStorage.getItem("selectedEntries"));
        localStorage.setItem("selectedEntries", JSON.stringify({...entries, [selectedTermId]: entryId}));
        dispatch({
            type: SELECT_LIST_ENTRY,
            termId: selectedTermId,
            entryId: entryId
        });
    }
};

export const selectSection = (sectionId, termId) => {
    return {
        type: VIEW_SECTION,
        sectionId: sectionId,
        termId: termId
    };
};

export const addToSelectedList = (termId, parentId, sectionId) => {
    return (dispatch) => {
        restCall("/lists/sections", {
            method: "POST",
            body: JSON.stringify({termId: termId, sectionId: sectionId, parentId: parentId})
        }, json => {
            dispatch({
                type: ADD_SECTION_TO_SCHEDULE_SUCCESSFUL,
                entryId: json.id,
                termId: termId,
                parentId: parentId,
                sectionId: sectionId
            });
            dispatch({
                type: SELECT_LIST_ENTRY,
                termId: termId,
                entryId: json.id
            })
        }, dispatch);
    }
};

export const changeParent = (entryId, newParentId) => {
    return (dispatch) => {
        restCall("/lists/" + entryId, {method: "PATCH", body: JSON.stringify({newParentId})}, json => {
            dispatch({
                type: MOVE_ENTRY,
                entryId,
                newParentId
            })
        })
    }
};

export const addNewSchedule = (scheduleName, termId) => {
    return (dispatch, getState) => {
        restCall("/lists/schedules", {
            method: "POST",
            body: JSON.stringify({termId: termId, scheduleName: scheduleName})
        }, json => {
                dispatch({
                    type: ADD_SCHEDULE_SUCCESSFUL,
                    entryId: json.id,
                    termId: termId,
                    scheduleName: scheduleName,
                    parentId: null
                });

                if(!getState().lists.selectedEntries[termId]){
                    dispatch({
                        type: SELECT_LIST_ENTRY,
                        termId: termId,
                        entryId: json.id
                    })
                }
        }, dispatch);
    }
};

export const addNewFolder = (termId, parentId, folderName) => {
    return (dispatch) => {
        restCall("/lists/groups", {
            method: "POST",
            body: JSON.stringify({termId: termId, folderName: folderName, parentId: parentId})
        }, json => {
            dispatch({
                type: ADD_FOLDER_SUCCESSFUL,
                entryId: json.id,
                folderName: folderName,
                parentId: parentId,
                termId: termId,
            });
        }, dispatch);
    }
};

export const updateScheduleName = (entryId, scheduleName) => {
    return (dispatch) => {
        restCall("/lists/schedules/"+entryId, {
            method: "PUT",
            body: JSON.stringify({scheduleName: scheduleName})
        }, json => dispatch({
            type: UPDATE_SCHEDULE_NAME_SUCCESSFUL,
            entryId: entryId,
            scheduleName: scheduleName,
        }), dispatch);
    }
};

export const updateGroupName = (entryId, name) => {
    return (dispatch) => {
        restCall("/lists/groups/"+entryId, {
            method: "PUT",
            body: JSON.stringify({groupName: name})
        }, json => dispatch({
            type: UPDATE_GROUP_NAME_SUCCESSFUL,
            entryId: entryId,
            groupName: name,
        }), dispatch);
    }
};

export const deleteSchedule = (entryId) => {
    return (dispatch, getState) => {
        restCall("/lists/schedules/"+entryId, {
            method: "DELETE"
        }, json => dispatch({
            type: DELETE_LIST_ENTRY_SUCCESSFUL,
            entryId: entryId,
            parentId: null,
            termId: getState().selectedTermId
        }), dispatch);
    }
};

export const deleteGroup = (entryId, parentId, termId) => {
    return (dispatch, getState) => {
        restCall("/lists/groups/"+entryId, {
            method: "DELETE"
        }, json => dispatch({
            type: DELETE_LIST_ENTRY_SUCCESSFUL,
            entryId: entryId,
            parentId: parentId,
            termId: getState().selectedTermId
        }), dispatch);
    }
};

export const removeSection = (entryId, parentId, termId) => {
    return (dispatch, getState) => {
        restCall("/lists/sections/"+entryId, {
            method: "DELETE"
        }, json => dispatch({
            type: DELETE_LIST_ENTRY_SUCCESSFUL,
            entryId: entryId,
            parentId:parentId,
            termId: getState().selectedTermId
        }), dispatch);
    }
};
