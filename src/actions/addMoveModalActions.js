import {HIDE_ADD_MOVE_MODAL, SHOW_ADD_ENTRY_MODAL, SHOW_MOVE_ENTRY_MODAL} from "./actionTypes";

export const showAddModal = (sectionId, disabledScheduleIds) => {
    return {
        type: SHOW_ADD_ENTRY_MODAL,
        sectionId: sectionId,
        disabledScheduleIds
    }
};

export const showMoveModal = (entryId, disabledScheduleIds) => {
    return {
        type: SHOW_MOVE_ENTRY_MODAL,
        entryId,
        disabledScheduleIds
    }
};

export const hideAddMoveModal = () => {
    return {
        type: HIDE_ADD_MOVE_MODAL
    }
};