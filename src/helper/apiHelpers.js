import {API, SERVER} from "./constants";
import {ADD_ERROR, LOGOUT} from "../actions/actionTypes";
import { hideLoading } from "react-redux-loading-bar";
import {Level} from "./constants"
import uuid from "uuid/v4"
export const restCall = (url, params, success, dispatch, hideLoadingOnFail) => {
    fetch(SERVER + API + url, {
        ...params,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        credentials: 'include'
    })
    .then(
        response => {
            /** @namespace response.ok */
            if (response.ok) {
                response.json().then(json => success(json));
            } else if (response.status === 401){
                dispatch({type: LOGOUT})
            } else {
                response.json().then(errorJson => {
                    dispatch({
                        type: ADD_ERROR,
                        id: uuid(),
                        ...errorJson
                    });
                    if (hideLoadingOnFail) dispatch(hideLoading());
                })
            }
        },
        error => {
            dispatch({
                type: ADD_ERROR,
                id: uuid(),
                level: Level.ERROR,
                error: "Network",
                message: "Unable to connect to the server at this time.",
                devMessage: error.toString()
            });
            if(hideLoadingOnFail) dispatch(hideLoading());
        }
    )
};