export const SERVER = (process.env.NODE_ENV === "production") ? "https://www.classifyregistration.com" : "http://localhost:8080";
export const API = "/api/v1";

export const MON = "MON";
export const TUE = "TUE";
export const WED = "WED";
export const THU = "THU";
export const FRI = "FRI";
export const SAT = "SAT";
export const SUN = "SUN";

export class ListEntryType {
    static SCHEDULE = "SCHEDULE";
    static GROUP = "GROUP";
    static SECTION = "SECTION";
}

export class Level {
    static INFO = "INFO";
    static WARN = "WARN";
    static ERROR = "ERROR";
}

export class SortBy {
    static SUBJECT = "Subject";
    static NAME = "Name";
    static PROFESSOR = "Professor";
}

export const SUPPORTED_UNIVERSITIES = ["UVM"];

export const JSON_HEADERS = {"Accept":"application/json", "Content-Type":"application/json"};