export const arraysIntersect = (array1, array2) => {
    let intersection = array1.filter(item => array2.indexOf(item) > -1);
    return intersection.length > 0;
};