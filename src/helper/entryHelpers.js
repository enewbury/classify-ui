import {ListEntryType} from "../helper/constants"

export const addEntryAndUpdateParent = (entries, entryId, parentId, newEntry) => {

    //add the new entry
    let updatedEntries = {...entries, [entryId]: newEntry};

    //if there is a parent, add the child, and update parent as well
    if (parentId !==null && parentId !== undefined) {
        let parent = entries[parentId];
        parent.entries.push(entryId);
        updatedEntries[parentId] = parent;
    }

    return updatedEntries
};

export const removeEntryAndUpdateParent = (entries, entryId, parentId) => {
    //make copy
    let updatedEntries = {...entries};

    //remove from parent entries list
    if (parentId !== null && parentId !== undefined) {
        let parent = entries[parentId];
        //remove from parent group entries
        updatedEntries[parent.id].entries = deleteFromArray(updatedEntries[parent.id].entries, entryId);
    }

    //remove all children entries
    updatedEntries = removeChildEntries(updatedEntries, entryId);

    //delete the entry
    delete updatedEntries[entryId];

    return updatedEntries
};

export const moveEntryAndUpdateParent = (entries, entryId, oldParentId, newParentId) => {
    //make copy
    let updatedEntries = {...entries};

    //set new parentId
    updatedEntries[entryId].parentId = newParentId;

    //remove from parent entries list
    if (oldParentId !== null && oldParentId !== undefined) {
        let parent = entries[oldParentId];
        //remove from parent group entries
        updatedEntries[parent.id].entries = deleteFromArray(updatedEntries[parent.id].entries, entryId);
    }

    //add to new parent
    if (newParentId !==null && newParentId !== undefined) {
        let parent = entries[newParentId];
        parent.entries.push(entryId);
    }

    return updatedEntries;
};

const removeChildEntries = (entries, entryId) => {
    if(entries[entryId].hasOwnProperty("entries")){
        entries[entryId].entries.forEach(childEntryId => {
            removeChildEntries(entries, childEntryId);
            delete entries[entryId];
        })
    }
    return entries;
};

export const deleteFromArray = (array, item) => {
    let index = array.indexOf(item);

    if(index === -1){
        return array;
    } else {
        return [
            ...array.slice(0, index),
            ...array.slice(index + 1)
        ]
    }
};

export const filterEntriesByAttributes = (entries, attributes) => {
    let filtered =  Object.keys(entries).reduce(function (filtered, key) {
        let matches = true;

        for(let attr in attributes) {
            if(!attributes.hasOwnProperty(attr) || entries[key][attr] !== attributes[attr]) {
                matches = false;
                break;
            }
        }

        if(matches) filtered[key] = entries[key];

        return filtered;
    }, {});

    return filtered;
};

const getComparisonText = (entry, courses, sections) => {
    switch(entry.type){
        case ListEntryType.SCHEDULE:
        case ListEntryType.GROUP:
            return entry.name;
        case ListEntryType.SECTION:
            if(sections && courses){
                let section = sections[entry.sectionId];
                let course = courses[section.courseId];
                return course.subject + course.courseNumber + section.sectionName;
            } else {
                return "";
            }

        default:
            return "";
    }
};

export const sortEntriesAlphabetically = (ids, entries, courses, sections) => {
    return ids.sort((idA, idB) => {
        let entryA = entries[idA];
        let entryB = entries[idB];

        return (getComparisonText(entryA, courses, sections) < getComparisonText(entryB, courses, sections)) ? -1 : 1;
    });
};

export const isContainerEntry = (type) => {
    switch (type){
        case ListEntryType.SCHEDULE:
        case ListEntryType.GROUP:
            return true;
        default:
            return false;
    }
};

export const getRootForEntry = (entry, entries) => {
    let curNode = entry;

    while(curNode && curNode.parentId !== null){
        curNode = entries[curNode.parentId];
    }

    return curNode;
};

export const getScheduleIdsForSection = (sectionId, term, entries) => {
    let sectionEntries = filterEntriesByAttributes(entries, {termId:term, type: ListEntryType.SECTION, sectionId: sectionId});
    let scheduleIds = [];
    Object.values(sectionEntries).forEach(sectionEntry => {
        let root = getRootForEntry(sectionEntry, entries);
        if(root) scheduleIds.push(root.id);
    });

    return scheduleIds;
};

