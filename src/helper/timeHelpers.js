import {MON, TUE, WED,THU, FRI, SAT, SUN} from "./constants"
export const timeToPercent = (time) => {
    return time.hour / 24 * 100 + time.minute / 60 / 24 * 100;
};

export const timeToString = (time, shorten=true) => {
    let [standardHour, suffix] = convertMilitaryHour(time.hour);
    if (shorten && (time.minute === null || parseInt(time.minute, 10) === 0)){
        return standardHour+suffix;
    } else {
        let minute = (time.minute < 10) ? "0"+time.minute : time.minute;
        return standardHour + ":" + minute + suffix;
    }
};

export const parseTimeString = (formattedTime) => {
    let segments = formattedTime.split(/:|AM|PM|am|pm/);
    if(segments.length < 3){
        throw new Error("Improperly formatted time string");
    } else {
        let hour = parseInt(segments[0], 10);
        let minute = parseInt(segments[1], 10);
        let suffix = segments[2].toLowerCase();
        if(suffix === "pm" && hour < 12){
            hour += 12;
        } else if(suffix === "am" && hour === 12){
            hour = 0;
        }

        return {hour, minute};
    }
};

export const timeToHours = (time) => {
    return time.hour + time.minute / 100;
};

export const hoursToTime = (hours) => {
    let hour = Math.floor(hours);
    let minute = Math.round((hours % 1) * 60);
    return {hour, minute};
};

export const convertMilitaryHour = (hour) => {
    let dec=parseInt(hour, 10);
    if(dec===0){
        return [12,"AM"];
    }
    else if(dec<12){
        return [dec,"AM"];
    }
    else if (dec===12){
        return [dec,"PM"];
    }
    else if(dec===24){
        return [12,"AM"];
    }
    else{
        return [dec-12,"PM"];
    }
};

export const convertToLongWeekday = (shortDay) => {
    switch(shortDay){
        case MON:
            return "Monday";
        case TUE:
            return "Tuesday";
        case WED:
            return "Wednesday";
        case THU:
            return "Thursday";
        case FRI:
            return "Friday";
        case SAT:
            return "Saturday";
        case SUN:
            return "Sunday";
        default:
            throw new Error("Unknown day of week " + shortDay);
    }
};
export const convertToShortWeekday = (day) => {
    switch(day) {
        case MON:
            return "M";
        case TUE:
            return "T";
        case WED:
            return "W";
        case THU:
            return "R";
        case FRI:
            return "F";
        case SAT:
            return "Sa";
        case SUN:
            return "Su";
        default:
            throw new Error("Unknown day of week " + day);
    }
};

export const overlaps = (event1, event2) => {
    if (event1 === null || event2 === null ) return false;
    let event1NumericStart = event1.startTime.hour + event1.startTime.minute / 100;
    let event2NumericStart = event2.startTime.hour + event2.startTime.minute / 100;
    let event1NumericEnd = event1.endTime.hour + event1.endTime.minute / 100;
    let event2NumericEnd = event2.endTime.hour + event2.endTime.minute / 100;

    return (
        //event 1 starts after event 2 starts, but before event 2 ends
        (event1NumericStart >= event2NumericStart && event1NumericStart < event2NumericEnd) ||
        //event 2 starts after event 1 starts, but before event 1 ends
        (event2NumericStart >= event1NumericStart && event2NumericStart < event1NumericEnd)
    );
};

export const isAfter = (timeA, timeB) => {
    return timeA.hour + timeA.minute / 100 > timeB.hour + timeB.minute / 100;
};
