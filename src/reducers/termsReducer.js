import {FETCH_TERMS_RECEIVED, FETCH_COURSE_DATA_RECEIVED
} from "../actions/actionTypes";
import {createReducer} from "./utilities";


const initialState = {
    isLoaded: false,
    entities: {},
    result: []
};

const termsReceived = (termsState, action) => {
    let termsById = {};
    action.terms.forEach((term) => termsById[term.id] = term);
    let allTermIds = action.terms.map(term => term.id);
    return {...termsState, isLoaded: true, entities: termsById, result: allTermIds};
};

const courseDataReceived = (termsState, action) => {
    let oldTerm = termsState.entities[action.termId];
    let newTerm = {...oldTerm, courseDataLoaded: true, sectionIds: Object.keys(action.courseData.entities.sections)};
    return {...termsState, entities: {...termsState.entities, [newTerm.id]:newTerm}};
};


export default createReducer(initialState, {
    [FETCH_TERMS_RECEIVED]: termsReceived,
    [FETCH_COURSE_DATA_RECEIVED]: courseDataReceived
})