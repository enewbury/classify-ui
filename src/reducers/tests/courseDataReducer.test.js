import {FETCH_COURSE_DATA_RECEIVED} from "../../actions/actionTypes";
import courseDataReducer from "../courseDataReducer";
const previousState = {
    isLoaded: true,
    entities: {
        courses:{0:"Course 1"},
        sections:{0:"Section 1"},
        instructors:{0:"Instructor 1"}
    },
    result:[0]
};
const action = {
    type: FETCH_COURSE_DATA_RECEIVED,
    courseData: {
        entities: {
            courses:{0:"Course 1", 1:"Course 2"},
            sections:{0:"Section 1", 1:"Section 2"},
            instructors:{0:"Instructor 1", 1: "Instructor 2"}
        },
        result:[0,1]
    }

};
const expectedState = {
    isLoaded: true,
    entities: {
        courses:{0:"Course 1", 1:"Course 2"},
        sections:{0:"Section 1", 1:"Section 2"},
        instructors:{0:"Instructor 1", 1: "Instructor 2"}
    },
    result:[0,1]
};

test("should have combined old with new", () => {
    const actualState = courseDataReducer(previousState, action);
    expect(actualState).toEqual(expectedState);
});
