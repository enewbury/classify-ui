import {normalize} from "normalizr";
import * as schema from "../../actions/schema";

const fromApi = [
    {
        "id": 1,
        "termId": 7,
        "userId": 1,
        "name": "Brainstorming Schedule",
        "childEntityIds": null,
        "entries": [
            {
                "id": 1,
                "entryId": 1,
                "parentId": null,
                "priority": 1,
                "sectionId": 17705,
                "group": false
            },
            {
                "id": 2,
                "entryId": 1,
                "parentId": null,
                "priority": 2,
                "sectionId": 18045,
                "group": false
            },
            {
                "id": 3,
                "entryId": 1,
                "parentId": null,
                "priority": 3,
                "name": "PE Class",
                "requiredCount": 1,
                "childEntityIds": null,
                "entries": [
                    {
                        "id": 5,
                        "entryId": 1,
                        "parentId": 3,
                        "priority": 1,
                        "sectionId": 21582,
                        "group": false
                    },
                    {
                        "id": 6,
                        "entryId": 1,
                        "parentId": 3,
                        "priority": 2,
                        "sectionId": 21610,
                        "group": false
                    },
                    {
                        "id": 7,
                        "entryId": 1,
                        "parentId": 3,
                        "priority": 3,
                        "sectionId": 21579,
                        "group": false
                    }
                ],
                "group": true
            },
            {
                "id": 4,
                "entryId": 1,
                "parentId": null,
                "priority": 4,
                "sectionId": 18222,
                "group": false
            }
        ]
    }
];

const expectedData = {};

it("should normalize correctly", () => {

    expect(normalize(fromApi, [schema.schedule])).toEqual(expectedData);
});