import * as types from '../actions/actionTypes';

export default (state = null, action) => {
    switch (action.type) {
        case types.SELECT_TERM:
            /** @namespace action.selectedTermId */
            return action.selectedTermId;

        default:
            return state
    }
};