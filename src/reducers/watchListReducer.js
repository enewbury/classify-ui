import * as types from '../actions/actionTypes';

export default (state = {isLoaded: false, entries: {}}, action) => {
    switch (action.type) {
        case types.WATCH_LIST_ENTRIES_RECEIVED:
            let entriesBySectionId = {};
            action.entries.forEach((entry) => entriesBySectionId[entry.sectionId] = entry);
            return {...state, isLoaded: true, entries: entriesBySectionId};

        case types.ADD_TO_WATCHLIST:
            return {...state, entries: {
                ...state.entries,
                [action.entry.sectionId]: action.entry
            }};

        case types.REMOVE_FROM_WATCHLIST_BY_SECTION:
            let updatedEntries = {...state.entries};
            delete updatedEntries[action.sectionId];
            return {...state, entries: updatedEntries};

        default:
            return state
    }
};