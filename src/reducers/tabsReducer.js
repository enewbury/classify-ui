import * as types from '../actions/actionTypes';
import {createReducer} from "./utilities";

const defaultTermState = {selectedTab: -1, openTabs: []};

const viewSection = (state, action) => {
    let termState = state[action.termId] || defaultTermState;
    let tabIndex = termState.openTabs.indexOf(action.sectionId);
    if (tabIndex > -1) {
        return {...state, [action.termId]:{selectedTab: tabIndex, openTabs: termState.openTabs}};
    } else {
        return {...state,
            [action.termId]:{selectedTab: termState.openTabs.length, openTabs: [...termState.openTabs, action.sectionId]}
        };
    }
};

const closeSectionTab = (state, action) => {
    let termState = state[action.termId] || defaultTermState;
    return {...state,
        [action.termId]: {
            selectedTab: action.index - 1,
            openTabs: [
                ...termState.openTabs.slice(0, action.index),
                ...termState.openTabs.slice(action.index + 1)
            ]
        }
    };
};

const selectSectionTab = (state,action) => {
    let termState = state[action.termId] || defaultTermState;
    return {...state, [action.termId]: {selectedTab: action.index, openTabs: termState.openTabs}};
};


export default createReducer({}, {
    [types.VIEW_SECTION]: viewSection,
    [types.CLOSE_SECTION_TAB]: closeSectionTab,
    [types.SELECT_SECTION_TAB]: selectSectionTab
})