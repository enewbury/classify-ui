export const updateItemInArray = (array, itemId, updateItemCallback) => {
    return array.map(item => {
        if (item.id !== itemId) {
            // Since we only want to update one item, preserve all others as they are now
            return item;
        }

        // Use the provided callback to create an updated item
        return updateItemCallback(item);
    });
};

export const createReducer = (initialState, handlers) => {
    return function reducer(state = initialState, action) {
        if (handlers.hasOwnProperty(action.type)) {
            return handlers[action.type](state, action)
        } else {
            return state
        }
    }
};

export class MultiMap {
    constructor(){
        this.map = new Map();
    }

    get(key){
        if (this.map.has(key)){
            return this.map.get(key);
        } else {
            return [];
        }
    }

    put(key, val){
        if(this.map.has(key)){
            this.map.get(key).push(val);
        } else {
            this.map.set(key, [val]);
        }
    }
}
