import * as types from '../actions/actionTypes';
import {deleteFromArray} from "../helper/entryHelpers";

export default (state = {errorIds: [], errors: {}}, action) => {
    switch (action.type) {
        case types.ADD_ERROR:
            return {
                ...state,
                errorIds: [...state.errorIds, action.id],
                errors: {
                    ...state.errors,
                    [action.id]: {id: action.id, error: action.error, message: action.message, devMessage: action.devMessage}
                }
            };

        case types.REMOVE_ERROR:
            let updatedErrors = {...state.errors};
            delete updatedErrors[action.id];

            return {
                ...state,
                errorIds: deleteFromArray(state.errorIds, action.id),
                errors: updatedErrors
            };

        default:
            return state
    }
};