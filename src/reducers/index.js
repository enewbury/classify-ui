import {combineReducers} from 'redux';
import { loadingBarReducer as loadingBar } from 'react-redux-loading-bar'
import terms from './termsReducer';
import user from "./userReducer";
import lists from "./listsReducer";
import search from "./searchReducer"
import watchList from "./watchListReducer";
import courseData from "./courseDataReducer";
import selectedTermId from "./selectedTermIdReducer";
import tabs from "./tabsReducer";
import addMoveModal from "./addMoveModalReducer"
import errors from "./errorsReducer"

const rootReducer = combineReducers({
    user,
    terms,
    lists,
    search,
    watchList,
    courseData,
    selectedTermId,
    tabs,
    loadingBar,
    addMoveModal,
    errors
});

export default rootReducer;