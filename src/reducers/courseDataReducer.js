import * as types from '../actions/actionTypes';

export default (state = {isLoaded: {}, entities: {courses:{}, sections:{}, instructors:{}}, result:[], sectionChanges: {}}, action) => {
    switch (action.type) {
        case types.FETCH_COURSE_DATA_RECEIVED:
            return {
                ...state,
                isLoaded: {...state.isLoaded, [action.termId]: true},
                entities: {
                    courses: {...state.entities.courses, ...action.courseData.entities.courses},
                    sections: {...state.entities.sections, ...action.courseData.entities.sections},
                    instructors: {...state.entities.instructors, ...action.courseData.entities.instructors}
                },
                result: [...new Set([...state.result, ...action.courseData.result])]
            };

        case types.SECTION_METRICS_RECEIVED:
            return {...state,
                sectionChanges: {...state.sectionChanges, [+action.sectionId]: action.changes}
            };

        default:
            return state
    }
};