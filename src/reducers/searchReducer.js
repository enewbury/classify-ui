import {FILTER_SEARCH} from "../actions/actionTypes";

export default (state = {
    title: "",
    subject: null,
    courseNumber: "",
    instructors: [],
    minInstructorScore: 0,
    days: [],
    includeEmptyTime: true,
    after: {hour: 6, minute: 0},
    before: {hour: 22, minute: 0}

}, action) => {
    switch (action.type) {
        case FILTER_SEARCH:
            return action.filters;
        default:
            return state;
    }
}