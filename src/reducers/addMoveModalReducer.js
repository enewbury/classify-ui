import * as types from '../actions/actionTypes';

export default (state = {sectionId: null, entryId: null, show: false, disabledScheduleIds: []}, action) => {
    switch (action.type){
        case types.SHOW_ADD_ENTRY_MODAL:
            return {...state, sectionId: action.sectionId, entryId: null, show: true, disabledScheduleIds: action.disabledScheduleIds};
        case types.SHOW_MOVE_ENTRY_MODAL:
            return {...state, sectionId: null, entryId: action.entryId, show: true, disabledScheduleIds: action.disabledScheduleIds};
        case types.HIDE_ADD_MOVE_MODAL:
            return {...state, sectionId: null, entryId: null, show: false, disabledScheduleIds: []};
        default:
            return state;
    }
}