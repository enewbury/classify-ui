import * as types from '../actions/actionTypes';
import {ListEntryType} from "../helper/constants"
import {
    addEntryAndUpdateParent, getRootForEntry, moveEntryAndUpdateParent,
    removeEntryAndUpdateParent
} from "../helper/entryHelpers";
import {createReducer} from "./utilities";

let selectedEntry = JSON.parse(localStorage.getItem("selectedEntries")) || {};
let defaultState = {selectedEntries: selectedEntry, entries: {}};

const schedulesReceived = (state, action) => {
    return {
        ...state,
        entries: {...state.entries, ...action.entries}
    };
};

const selectListEntry = (state, action) => {
    return {
        ...state,
        selectedEntries: {...state.selectedEntries, [action.termId]: action.entryId}
    };
};

const addSection = (state, action) => {
    let newEntry = {
        id: action.entryId,
        termId: action.termId,
        parentId: action.parentId,
        type: ListEntryType.SECTION,
        priority: 5,
        sectionId: action.sectionId
    };

    return {
        ...state,
        entries: addEntryAndUpdateParent(state.entries, action.entryId, action.parentId, newEntry)
    };
};

const addSchedule = (state, action) => {
    let newEntry = {
        id: action.entryId,
        termId: action.termId,
        parentId: null,
        type: ListEntryType.SCHEDULE,
        priority: 5,
        name: action.scheduleName,
        entries: []
    };
    return {
        ...state,
        entries: addEntryAndUpdateParent(state.entries, action.entryId, action.parentId, newEntry)
    };
};

const addFolder = (state, action) => {
    let newEntry = {
        id: action.entryId,
        termId: action.termId,
        parentId: action.parentId,
        type: ListEntryType.GROUP,
        priority: 5,
        name: action.folderName,
        requiredCount: 1,
        entries: []
    };

    return {
        ...state,
        entries: addEntryAndUpdateParent(state.entries, action.entryId, action.parentId, newEntry)
    };
};

const updateScheduleName = (state, action) => {
    return {
        ...state,
        entries: {
            ...state.entries,
            [action.entryId]: {
                ...state.entries[action.entryId],
                name: action.scheduleName
            }
        }
    }
};

const updateGroupName = (state, action) => {
    return {
        ...state,
        entries: {
            ...state.entries,
            [action.entryId]: {
                ...state.entries[action.entryId],
                name: action.groupName
            }
        }
    }
};

const moveEntry = (state, action) => {
    return {
        ...state,
        entries: moveEntryAndUpdateParent(
            state.entries,
            action.entryId,
            state.entries[action.entryId].parentId,
            action.newParentId)
    }
};

const deleteEntry = (state, action) => {
    let selectedEntryId = (state.selectedEntries[action.termId] === action.entryId) ?
        (getRootForEntry(state.entries[action.entryId], state.entries) || {id: null}).id :
        state.selectedEntries[action.termId];
    return {
        ...state,
        selectedEntries: {...state.selectedEntries, [action.termId]: selectedEntryId},
        entries: removeEntryAndUpdateParent(state.entries, action.entryId, action.parentId)
    }
};


export default createReducer(defaultState, {
    [types.FETCH_SCHEDULES_RECEIVED]: schedulesReceived,
    [types.SELECT_LIST_ENTRY]: selectListEntry,
    [types.ADD_SECTION_TO_SCHEDULE_SUCCESSFUL]: addSection,
    [types.ADD_SCHEDULE_SUCCESSFUL]: addSchedule,
    [types.ADD_FOLDER_SUCCESSFUL]: addFolder,
    [types.UPDATE_SCHEDULE_NAME_SUCCESSFUL]: updateScheduleName,
    [types.UPDATE_GROUP_NAME_SUCCESSFUL]: updateGroupName,
    [types.MOVE_ENTRY]: moveEntry,
    [types.DELETE_LIST_ENTRY_SUCCESSFUL]: deleteEntry
});
