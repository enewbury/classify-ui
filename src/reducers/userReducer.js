import * as types from '../actions/actionTypes';

export default (state = {
    user: {},
    authenticated: false,
    loginProcessing: false,
    loginError:null,

    registrationProcessing: false,
    registrationComplete: false,
    registrationError: null
    }, action) => {
    switch (action.type) {
        case types.ATTEMPTING_LOGIN:
            return {...state, loginProcessing: true};

        case types.LOGIN_SUCCESSFUL:
            let newState = {...state, loginProcessing: false, loginError: null, authenticated: true, user: action.user};
            sessionStorage.setItem("user", JSON.stringify(newState));
            return newState;

        case types.LOGIN_FAILED:
            return {...state, loginProcessing: false, loginError: {message: action.message, exception: action.exception}};

        case types.LOGOUT:
            let loggedOutState = {...state, authenticated: false, registrationComplete: false, user: {}};
            sessionStorage.setItem("user", JSON.stringify(loggedOutState));
            return loggedOutState;

        case types.ATTEMPT_REGISTER:
            return {...state, registrationProcessing: true};

        case types.REGISTER_SUCCESSFUL:
            return {...state, registrationProcessing: false, registrationComplete: true, registrationError: null};

        case types.REGISTER_FAILED:
            return {...state, registrationProcessing: false, registrationError: {message: action.message, exception: action.exception}};

        case types.NAME_UPDATED:
            let updatedNameState = {...state, user:{...state.user, firstName: action.firstName, lastName: action.lastName}};
            sessionStorage.setItem("user", JSON.stringify(updatedNameState));
            return updatedNameState

        case types.EMAIL_UPDATED:
            let updatedEmailState = {...state, user: {...state.user, email: action.email}};
            sessionStorage.setItem("user", JSON.stringify(updatedEmailState));
            return updatedEmailState;

        default:
            return state
    }
};