import rootReducer from '../reducers';
import {createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk'

/** @namespace window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ */
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
    let initialState = {};
    let user = JSON.parse(sessionStorage.getItem("user"));
    if (user){
        initialState.user = user;
    }
    return createStore(rootReducer, initialState, composeEnhancers(applyMiddleware(thunk)));
};