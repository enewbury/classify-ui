import React, {Component} from 'react';
import "./Home.css"
import watchIcon from "./watchIcon.png"
import searchIcon from "./searchIcon.png"
import compareIcon from "./compareIcon.png"
import calendar from "./calendar.jpg"
import info from "./info.jpg"
import search from "./search.jpg"
import email from "./email.png"

import SignUp from "../auth/SignUp";
import {Col, Row} from "react-bootstrap";
import Footer from "../layout/Footer/Footer";

class Home extends Component {
    render() {
        return (
            <div className="full-height-auto">
                <div className="home-splash clear">
                    <div className="center clear">
                        <div className="welcome">
                            <h1>Your Registration Companion</h1>
                            <p>Get organized and never miss out on getting into a class again.
                                Classify keeps tabs on the enrollment status of all your favorite classes so you don't have to.</p>

                            <section className="features clear">
                                <div className="feature">
                                    <img src={watchIcon} alt="Watch Classes" />
                                        <span>Get notified after course enrollment changes</span>
                                </div>

                                <div className="feature">
                                    <img src={searchIcon} alt="Search for Classes" />
                                        <span>Advanced searching features for classes.</span>
                                </div>

                                <div className="feature">
                                    <img src={compareIcon} alt="Compare Classes" />
                                        <span>Compare classes side by side in a nifty weekly schedule.</span>
                                </div>

                            </section>
                        </div>
                        <div className="float-right signup-container">
                            <SignUp />
                        </div>
                    </div>
                </div>

                <Row className="center" style={{paddingTop: 100, paddingBottom: 100}}>
                    <Col md={5} className="feature-description">
                        <h1>Organized Course Info</h1>
                        <p>Find everything you need to know about a course all in one place!  Compare courses side by side with up to date enrollment numbers, rate my professor scores, and full course descriptions.</p>
                    </Col>
                    <Col md={7}><img className="feature-img" src={info} alt="Info display" /></Col>
                </Row>

                <div style={{backgroundColor: "#e5e5e5", borderTop: "solid 4px  #ddd", borderBottom: "solid 4px #ddd"}}>
                    <Row className="center" style={{paddingTop: 100, paddingBottom: 100}}>
                        <Col md={7}><img className="feature-img" src={calendar} alt="Calendar display" /></Col>
                        <Col md={5} className="feature-description">
                            <h1>Weekly Calendar</h1>
                            <p>View your schedule of courses on a weekly calendar color coded by subject.  Classify makes your schedule part of the planning process, not an after thought.</p>
                        </Col>
                    </Row>
                </div>

                <Row className="center" style={{paddingTop: 100, paddingBottom: 100}}>
                    <Col md={5} className="feature-description">
                        <h1>Advanced Search</h1>
                        <p>Need to fit one more course in after lunch on Mondays, or want to take an elective with a highly rated professor?  Use classifies advanced searching features to browse courses by the many filtering tools.</p>
                    </Col>
                    <Col md={7}><img className="feature-img" src={search} alt="search display" /></Col>
                </Row>

                <div style={{backgroundColor: "#e5e5e5", borderTop: "solid 4px  #ddd", borderBottom: "solid 4px #ddd"}}>
                    <Row className="center" style={{paddingTop: 100, paddingBottom: 100}}>
                        <Col md={7} style={{textAlign: "center"}}><img className="feature-img" src={email} alt="Notification Email" /></Col>
                        <Col md={5} className="feature-description">
                            <h1>Course Monitoring</h1>
                            <p>Let us email you as soon as that full class has an opening.  Don't worry about checking back every 5 minutes to see if someone dropped the class. We'll let you know as soon as their's space for you.</p>
                        </Col>
                    </Row>
                </div>

                <Footer />

            </div>
        );
    }
}

Home.propTypes = {};
Home.defaultProps = {};

export default Home;