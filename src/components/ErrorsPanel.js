import React, {Component} from 'react';
import {Panel} from "react-bootstrap"
import {FaClose} from "react-icons/lib/fa"
import {connect} from "react-redux";
import {REMOVE_ERROR} from "../actions/actionTypes";

class ErrorsPanel extends Component {
    render() {
        return (
            <div>
                {this.props.errorIds.map(id =>
                    <Panel key={id} header={
                        <span>
                            <b>Error</b><button onClick={() => this.props.actions.removeError(id)} className="float-right"><FaClose/></button>
                        </span>
                    } style={{boxShadow: "1px 5px 20px rgba(68, 68, 68, 0.17)"}} bsStyle="danger"
                    >
                        {this.props.errors[id].message}
                    </Panel>
                )}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        errorIds: state.errors.errorIds,
        errors: state.errors.errors
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            removeError: (errorId) => dispatch({type: REMOVE_ERROR, id: errorId})
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ErrorsPanel);