import React, {Component} from 'react';
import {Modal, Button} from "react-bootstrap"
import {FaListUl, FaFolder, FaAngleRight, FaAngleLeft, FaPlus, FaArrows} from "react-icons/lib/fa"
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {ListEntryType} from "../../helper/constants"
import {
    filterEntriesByAttributes, sortEntriesAlphabetically, isContainerEntry,
    getRootForEntry
} from "../../helper/entryHelpers";
import "./AddMoveModal.css"
import {addToSelectedList, changeParent} from "../../actions/listsActions";
import {hideAddMoveModal} from "../../actions/addMoveModalActions";

class AddMoveModal extends Component {

    constructor(props){
        super(props);
        this.state = {children: [], currentEntry: null, selectedEntry: null};
        this.enterEntry = this.enterEntry.bind(this);
        this.goBack = this.goBack.bind(this);
        this.addOrMove = this.addOrMove.bind(this);
    }

    componentWillMount(){
        this.loadState(this.props)
    }

    componentWillReceiveProps(props){
        this.loadState(props);
    }

    loadState(props){
        if(props.courseDataLoaded && props.selectedTermId !== null) {
            this.setState({
                children: Object.keys(filterEntriesByAttributes(props.listEntries, {termId: props.selectedTermId, parentId: null})),
                currentEntry: null,
                selectedEntry: this.props.selectedSchedule
            });
        }
    }

    enterEntry(entry){
        this.setState({
            currentEntry: entry,
            selectedEntry: entry,
            children: Object.keys(filterEntriesByAttributes(this.props.listEntries, {
                termId: this.props.selectedTermId, parentId: entry.id
            }))
        });
    }

    goBack(){
        let newCurrentEntry = this.props.listEntries[this.state.currentEntry.parentId] || null;
        let newParentId = (newCurrentEntry && this.props.listEntries[newCurrentEntry.parentId]) ? this.props.listEntries[newCurrentEntry.parentId].parentId : null;
        this.setState({
            currentEntry: newCurrentEntry,
            selectedEntry: this.state.currentEntry,
            children: Object.keys(filterEntriesByAttributes(this.props.listEntries, {termId: this.props.selectedTermId, parentId: newParentId}))
        });
    }

    addOrMove(){
        this.props.actions.hideModal()
        if(this.props.entryId){
            this.props.actions.changeParent(this.props.entryId, this.state.selectedEntry.id);
        } else {
            this.props.actions.addToList(this.props.selectedTermId, this.state.selectedEntry.id, this.props.sectionId);
        }
    }

    render() {
        let sortedEntryIds = sortEntriesAlphabetically(this.state.children, this.props.listEntries, this.props.courses, this.props.sections);
        return (
            <Modal className="AddMoveModal" show={this.props.visible} onHide={this.props.actions.hideModal}>
                <Modal.Header closeButton>{(this.state.currentEntry) ? <b><FaAngleLeft onClick={this.goBack} className="back-button"/>{this.state.currentEntry.name}</b> : <b>My Schedules</b>}</Modal.Header>
                    <div className="list-modal-lists">
                        {sortedEntryIds.map(childId => {
                            let child = this.props.listEntries[childId];
                            let isDisabled = !isContainerEntry(child.type) || this.props.disabledScheduleIds.includes(child.id);
                            let innerComponent;
                            switch (child.type){
                                case ListEntryType.SCHEDULE:
                                    innerComponent = <span>{!isDisabled &&
                                    <FaAngleRight onClick={(e) => this.enterEntry(child)} className="float-right enter-entry-button"/>}
                                    <FaListUl style={{marginRight: 10}}/><b>{this.props.listEntries[childId].name}</b></span>;
                                    break;
                                case ListEntryType.GROUP:
                                    innerComponent = <span>{!isDisabled &&
                                        <FaAngleRight onClick={(e) => this.enterEntry(child)} className="float-right enter-entry-button"/>}
                                        <FaFolder style={{marginRight: 10}}/><b>{this.props.listEntries[childId].name}</b>
                                    </span>;
                                    break;
                                case ListEntryType.SECTION:
                                    let section = this.props.sections[this.props.listEntries[childId].sectionId];
                                    let course = this.props.courses[section.courseId];
                                    innerComponent = <span>{course.subject + " " + course.courseNumber + " - " + section.sectionName}</span>;
                                    break;
                                default:
                                    innerComponent = null;
                            }
                            let classNameValue = "list-modal-item";
                            classNameValue += (this.state.selectedEntry && this.state.selectedEntry.id === child.id) ? " selected" : "";
                            classNameValue += isDisabled ? " disabled" : "";
                            return <div key={child.id} className={classNameValue}
                                        onClick={isDisabled ? () => {} : () => this.setState({selectedEntry: child})}>
                                    {innerComponent}
                                </div>
                        })}
                    </div>

                <Modal.Footer>
                    <Button onClick={this.addOrMove} disabled={this.state.selectedEntry === null} bsSize="sm" bsStyle="primary">{(this.props.entryId) ? <span><FaArrows /> Move Here</span> : <span><FaPlus /> Add Here</span>}</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

AddMoveModal.propTypes = {
    disabledScheduleIds: PropTypes.array
};
AddMoveModal.defaultProps = {disabledScheduleIds: []};

const mapStateToProps = (state) => {
    return {
        sectionId: state.addMoveModal.sectionId,
        entryId: state.addMoveModal.entryId,
        visible: state.addMoveModal.show,
        disabledScheduleIds: state.addMoveModal.disabledScheduleIds,
        courseDataLoaded: state.courseData.isLoaded[state.selectedTermId] === true,
        courses: state.courseData.entities.courses,
        sections: state.courseData.entities.sections,
        selectedSchedule: getRootForEntry(state.lists.entries[state.lists.selectedEntries[state.selectedTermId]], state.lists.entries),
        listEntries: state.lists.entries,
        selectedTermId: state.selectedTermId
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            hideModal: () => dispatch(hideAddMoveModal()),
            addToList: (termId, parentId, sectionId) => dispatch(addToSelectedList(termId, parentId, sectionId)),
            changeParent: (entryId, newParentId) => dispatch(changeParent(entryId, newParentId))
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(AddMoveModal);