import React, {Component} from 'react';
import "./Radiator.css"
import PropTypes from 'prop-types';

class Radiator extends Component {
    constructor(props){
        super(props);
        let percentage = (!isNaN(props.value) && !isNaN(props.maxValue)) ? props.value/props.maxValue*100 : props.percentage;
        this.state = {
            percentage: props.initialAnimation ? 0 : percentage,
            value: props.initialAnimation ? 0 : props.value,
            maxValue: props.maxValue
        };
    }

    componentDidMount() {
        if (this.props.initialAnimation) {
            let percentage = (!isNaN(this.props.value) && !isNaN(this.props.maxValue)) ? this.props.value/this.props.maxValue*100 : this.props.percentage;
            this.initialTimeout = setTimeout(() => {
                this.requestAnimationFrame = window.requestAnimationFrame(() => {
                    this.setState({
                        percentage: percentage,
                        value: this.props.value
                    });
                });
            }, 0);
        }
    }

    componentWillReceiveProps(nextProps) {
        let percentage = (!isNaN(nextProps.value) && !isNaN(nextProps.maxValue)) ? nextProps.value/nextProps.maxValue*100 : nextProps.percentage;
        this.setState({
            percentage: percentage,
            value: nextProps.value
        });
    }

    componentWillUnmount() {
        clearTimeout(this.initialTimeout);
        window.cancelAnimationFrame(this.requestAnimationFrame);
    }

    render(){
        let proportion = (this.props.invert) ? Math.min(this.state.percentage, 100)/100 : 1-Math.min(this.state.percentage, 100)/100;
        let color = "hsl("+ 200*proportion+", 70%, 50%)";

        const radius = (50 - this.props.stroke / 2);
        const pathDescription = `
        M 50,50 m 0,-${radius} 
        a ${radius},${radius} 0 1 1 0,${2 * radius} 
        a ${radius},${radius} 0 1 1 0,-${2 * radius}
        `;

        const diameter = Math.PI * 2 * radius;
        const progressStyle = {
            stroke: color,
            strokeDasharray: `${diameter}px ${diameter}px`,
            strokeDashoffset: `${((100 - Math.min(this.state.percentage, 100)) / 100 * diameter)}px`,
        };

        return (
            <svg className={(!this.props.animation) ? "no-animation Radiator" : "Radiator"} viewBox="0 0 100 100">
                <path className="Radiator-trail" d={pathDescription} strokeWidth={this.props.stroke - 0.5} fillOpacity={0}/>
                <path className="Radiator-path" d={pathDescription} strokeWidth={this.props.stroke} fillOpacity={0}
                      style={progressStyle}
                />
                {this.props.showText &&
                <text className="Radiator-text" x="50"
                      y="50">{(this.props.value && this.props.maxValue) ? this.props.value + "/" + this.props.maxValue : (!isNaN(this.state.percentage)) ? Math.round(this.state.percentage) + "%" : "N/A"}</text>
                }
            </svg>
        );
    }
}

Radiator.propTypes = {animation: PropTypes.bool, showText: PropTypes.bool, invert: PropTypes.bool, value: PropTypes.number, maxValue: PropTypes.number, percentage: PropTypes.number, stroke: PropTypes.number, initialAnimation: PropTypes.bool};
Radiator.defaultProps = {animation: true, showText: true, stroke: 5, initialAnimation: true, invert: false};

export default Radiator;