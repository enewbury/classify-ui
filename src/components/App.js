import React, { Component } from 'react'
import Header from "./layout/Header/Header"
import {BrowserRouter as Router, Route, Switch} from "react-router-dom"
import ErrorsPanel from "./ErrorsPanel"
import {configureAppState} from "./configureAppState";
import Login from "./auth/Login";
import ListsView from "./lists/ListsView";
import ProtectedRoute from "./auth/ProtectedRoute";
import SignUp from "./auth/SignUp";
import Verify from "./auth/Verify";
import Forgot from "./auth/Forgot";
import Reset from "./auth/Reset";
import SearchView from "./search/SearchView";
import Home from "./Home/Home";
import VerifyNewEmail from "./auth/VerifyNewEmail";
import About from "./About/About";

class App extends Component {
    render() {
        return (
            <Router>
                <div className="App">
                    <Header/>
                    <div id="main-content">
                        <Switch>
                            <Route exact path="/login" component={Login} />
                            <Route exact path="/sign-up" component={SignUp} />
                            <Route exact path="/verify" component={Verify} />
                            <Route exact path="/update-email" component={VerifyNewEmail} />
                            <Route exact path="/forgot" component={Forgot} />
                            <Route exact path="/reset" component={Reset} />
                            {/*<Route exact path="/donate" component={Donate}/>*/}
                            <Route exact path="/about" component={About} />
                            <ProtectedRoute path="/lists" component={ListsView} />
                            <ProtectedRoute path="/search" component={SearchView} />

                            <Route path="/" component={Home}/>
                        </Switch>
                    </div>
                    <div style={{position: "fixed", top: 30, right: 15, maxWidth: "calc(100% - 30)", width: 400, zIndex: 5}}><ErrorsPanel /></div>
                    {/*<DonationReminder />*/}
                </div>
            </Router>
        );
    }
}

export default configureAppState(App);
