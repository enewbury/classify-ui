import "./Schedule.css";
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Clamp from "../layout/Clamp"
import {isAfter, overlaps, timeToPercent, timeToString} from "../../helper/timeHelpers"

class ScheduleDay extends Component {




    compileGroupingData(events){
        if (events === null || events.length === 0) return {};
        let sortedEvents = events.sort(function (a, b) {
            return timeToPercent(a.startTime) - timeToPercent(b.startTime);
        });
        
        let index = 0;
        let columnGroups = [];
        while (index < sortedEvents.length){
            let {columns, newIndex} = this.getNextGroup(sortedEvents, index);
            index=newIndex;
            columnGroups.push(columns);
        }
        
        const groupingDataMap = {};

        columnGroups.forEach(columns => {
            let width = 1 / columns.length;
            columns.forEach((column, i) => {
                let data = {width: width * 100 +"%", left: width * i * 100 + "%"};
                column.forEach(event => {
                    groupingDataMap[event.id] = data;
                });
            });
        });

        return groupingDataMap;
    }

    getNextGroup(events, index){
        let newIndex = index;
        let lastEndTime = null;
        let columns = [];
        for(let i = index; i < events.length; i++) {
            let event = events[i];
            if(lastEndTime !== null && isAfter(event.startTime, lastEndTime)){
                return {columns, newIndex};
            }

            newIndex = i+1;
            let placed = false;
            
            for(let column of columns) {
                //event doesn't collide with last item in column
                if(!overlaps(event, column[column.length - 1])){
                    column.push(event);
                    placed = true;
                    break;
                }
            }

            //couldn't fit in any existing column, make a new one
            if(!placed){
                columns.push([event]);
            }
            
            lastEndTime = (lastEndTime === null || isAfter(event.endTime, lastEndTime)) ? event.endTime : lastEndTime;
        }
        
        return {columns, newIndex}
    }

    generateEvents(){
        let groupingDataMap = this.compileGroupingData(this.props.events);
        return this.props.events.map(timeSlot => {
            let groupingData = groupingDataMap[timeSlot.id];
            return <div key={timeSlot.id} className="schedule-event" style={{
                height: timeToPercent(timeSlot.endTime) - timeToPercent(timeSlot.startTime) + "%",
                top: timeToPercent(timeSlot.startTime)+"%",
                width: `calc(${groupingData.width} - 2px)`,
                left: groupingData.left,
                backgroundColor: "rgba("+timeSlot.rgb+",0.5)",
                borderColor: "rgba("+timeSlot.rgb+",0.6)"
            }}>
                <div style={{height: "100%", overflow: "hidden"}}>
                    <Clamp clamp={"auto"}>{timeSlot.title}<br /><small style={{color: "#666"}}>{timeToString(timeSlot.startTime)}</small></Clamp>
                </div>
            </div>
        });
    }


    render() {
        return (
            <div style={{width: this.props.width}} className="schedule-day-slot">
                {this.generateEvents()}
            </div>
        );
    }
}

ScheduleDay.propTypes = {temp: PropTypes.array, displayDays: PropTypes.array.isRequired, events: PropTypes.array, width: PropTypes.string};
ScheduleDay.defaultProps = {temp: [0], events: [], width: "100%"};

export default ScheduleDay;