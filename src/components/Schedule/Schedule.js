import "./Schedule.css"
import ScheduleDay from "./ScheduleDay"
import * as helpers from "../../helper/timeHelpers";
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {convertToShortWeekday} from "../../helper/timeHelpers";
import {convertToLongWeekday} from "../../helper/timeHelpers";
import {FRI, MON, SAT, SUN, THU, TUE, WED} from "../../helper/constants";

class Schedule extends Component {

    constructor(props){
        super(props);
        this.calculateDaySlots = this.calculateDaySlots.bind(this);
        this.renderChildren = this.renderChildren.bind(this);
        this.renderHeaders = this.renderHeaders.bind(this);

        this.state = this.calculateDaySlots(props)
    }

    componentDidMount(){
        this.setScrollHeight();
    }

    componentWillReceiveProps(nextProps){
        let nextState = this.calculateDaySlots(nextProps);
        this.setState(nextState);
    }

    componentDidUpdate(){
        this.setScrollHeight();
    }

    calculateDaySlots(props = this.props){
        let children = React.Children.toArray(props.children);
        let includeWeekend = this.eventsIncludeWeekend(props.events);
        if(children.length === 0){
            if (includeWeekend) children.push(<ScheduleDay displayDays={[SUN]}/>);
            children.push(<ScheduleDay displayDays={[MON]}/>);
            children.push(<ScheduleDay displayDays={[TUE]}/>);
            children.push(<ScheduleDay displayDays={[WED]}/>);
            children.push(<ScheduleDay displayDays={[THU]}/>);
            children.push(<ScheduleDay displayDays={[FRI]}/>);
            if (includeWeekend) children.push(<ScheduleDay displayDays={[SAT]}/>);
        }

        return {children, includeWeekend};
    }

    setScrollHeight(){
        let earliestEvent = null;
        this.props.events.forEach(event => {
            if(earliestEvent === null || earliestEvent > event.startTime.hour) earliestEvent = event.startTime.hour;
        });

        let scrollFraction = Math.max(0, earliestEvent / 24 - .04);
        this.scrollableContainer.scrollTop = this.container.scrollHeight * scrollFraction;
    }

    createHourSlots(){
        return [...new Array(24)].map((x,i) => {
            return <div className="hour-slot" style={{top: (i)/24*100 + "%"}} key={i}/>
        });
    }

    createTimeLabels(){
        let offset = 10;
        return [...new Array(24)].map((x,i) => {
            return <span key={i} className="time-label" style={{top: "calc("+(i)/24*100 + "% - " + offset + "px)"}}>{helpers.timeToString({hour:i, minute: 0})}</span>
        });
    }

    renderHeaders(){
        if(this.props.showHeaders){
            let width = 1 / this.state.children.length * 100 + "%";
            return <div className="schedule-header-container">
                <div style={{width: 40, height: "100%", float: "left"}} />
                <div style={{overflow: "hidden"}}>
                    {this.state.children.map((child,i) => {
                        let days = (child.props.displayDays.length > 1) ?
                            child.props.displayDays.map(d => convertToShortWeekday(d)).join("") :
                            child.props.displayDays.map(d => convertToLongWeekday(d)).join("");
                        return <div key={i} className="schedule-header-section" style={{width: width}}>{days}</div>
                    })}
                </div>
            </div>
        } else {
            return null;
        }
    }

    eventsIncludeWeekend(events){
        //check for any SAT or SUN events
        for (const event of events){
            if (event.daysList.includes(SUN) || event.daysList.includes(SAT)){
                return true;
            }
        }

        return false;
    }

    renderChildren() {
        let width = 1 / this.state.children.length * 100 + "%";
        return this.state.children.map((child,i) => {
            //select events which overlap in displayDays and the days of this timeSlot
            let filteredEvents = this.props.events.filter(event => {
                let intersection = child.props.displayDays.filter(day => event.daysList.indexOf(day) > -1);
                return intersection.length > 0;
            });
            return React.cloneElement(child, {
                events: filteredEvents,
                width: width,
                key: i
            });
        })
    }

    render() {
        return (
            <div style={{height: "100%"}}>
                {this.renderHeaders()}
                <div className="scrollable-container" style={{height: (this.props.showHeaders) ? "calc(100% - 40px)" : "100%"}} ref={(scrollableContainer) => {this.scrollableContainer = scrollableContainer;}}>
                    <div className="schedule-container" ref={container => {this.container = container;}}>
                        <div className="schedule-time-labels">{this.createTimeLabels()}</div>
                        <div className="schedule-main-area">
                            {this.createHourSlots()}
                            {this.renderChildren()}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Schedule.propTypes = {events: PropTypes.array.isRequired, showHeaders: PropTypes.bool};
Schedule.defaultProps = {showHeaders: true};

export default Schedule;