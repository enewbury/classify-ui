import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Button, FormControl, FormGroup, InputGroup, Modal, Panel} from "react-bootstrap";
import {API, SERVER} from "../../helper/constants";
import {create} from "braintree-web-drop-in";

class DonateModal extends Component {
    constructor(props){
        super(props);
        this.state = {processing: true, status: null, message: "", amountValue: "", emailValue: ""};
        this.initializeBraintree = this.initializeBraintree.bind(this);
        this.processDonation = this.processDonation.bind(this);
    }


    initializeBraintree(){
        create({
            authorization: this.props.clientToken,
            container: '#braintree-container'
        }, (createErr, instance) => {
            this.setState({processing: false});
            document.getElementById("braintree-submit").addEventListener("click", () => {
                instance.requestPaymentMethod((err, payload) => {
                    this.processDonation(payload.nonce);
                });
            });
        });
    }

    processDonation(nonce){
        this.setState({processing: true});
        fetch(SERVER + API + "/payments/donation", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                amount: Number.parseInt(this.state.amountValue, 10),
                email: this.state.emailValue,
                nonce: nonce
            })
        }).then(
            response => {
                if (response.ok) {
                    this.setState({processing: false, status: "ok", message: "Thank You! We successfully processed your donation."})
                } else {
                    response.json().then(json => {
                        this.setState({processing: false, status: "failed", message: json.message});
                    });
                }
            },
            errorResponse => {
                this.setState({processing: false, status: "failed", message: "Unfortunately we couldn't connect to the server to process your donation"});
            }
        );
    }

    render() {
        return (
            <Modal show={this.props.show} onHide={this.props.close} onEntered={this.initializeBraintree}>
                <Modal.Header closeButton><Modal.Title>Donate</Modal.Title></Modal.Header>
                <Modal.Body>
                    {this.state.status !== null && <Panel bsStyle={(this.state.status === "ok") ? "info" : "danger"}>{this.state.message}</Panel>}
                    <FormGroup>
                        <InputGroup>
                            <InputGroup.Addon>$</InputGroup.Addon>
                            <FormControl placeholder="18" style={{textAlign: "right"}} type="text" bsSize="lg" value={this.state.amountValue} onChange={(e) => this.setState({amountValue: e.target.value})}/>
                            <InputGroup.Addon>.00</InputGroup.Addon>
                        </InputGroup>
                    </FormGroup>
                    <FormGroup>
                        <InputGroup>
                            <InputGroup.Addon>@</InputGroup.Addon>
                            <FormControl placeholder="myusername@university.edu" type="text" value={this.state.emailValue} onChange={(e) => this.setState({emailValue: e.target.value})}/>
                        </InputGroup>
                        <small>Provide your university email so that we remember that you donated and can give you free access once Classify becomes a paid service.</small>
                    </FormGroup>

                    <div id="braintree-container"/>
                </Modal.Body>
                <Modal.Footer><Button disabled={this.state.processing} bsStyle="primary" id="braintree-submit">Submit</Button></Modal.Footer>
            </Modal>
        );
    }
}

DonateModal.propTypes = {clientToken: PropTypes.string, show: PropTypes.bool, close: PropTypes.func};
DonateModal.defaultProps = {};

export default DonateModal;