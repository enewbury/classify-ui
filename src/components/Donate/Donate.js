import React, {Component} from 'react';
import eric from "./face.jpg"
import "./Donate.css"
import {Button, Image} from "react-bootstrap";
import {API, SERVER} from "../../helper/constants";
import DonateModal from "./DonateModal";

class Donate extends Component {
    constructor(props){
        super(props);
        this.state = {modalOpen: false, clientToken: null};
        this.getClientTokenAndInitializeBraintree = this.getClientTokenAndInitializeBraintree.bind(this);
        this.getClientTokenAndInitializeBraintree();
    }

    getClientTokenAndInitializeBraintree(){
        fetch(SERVER + API + "/payments/braintreeToken", {headers:{"Content-Type": "application/json"}}).then(
            response => {
                if (response.ok) {
                    response.json().then(json => {
                        this.setState({processing: false, clientToken: json.clientToken});
                    });

                } else {
                    response.json().then(json => {
                        this.setState({status: "failed", message: json.message});
                    });
                }
            },
            errorResponse => {
                this.setState({status: "failed", message: "Unfortunately we couldn't connect to the server to process your donation"});
            }
        );
    }

    render() {
        return (
            <div className="Donate full-height-auto">
                {this.state.clientToken && <DonateModal clientToken={this.state.clientToken} show={this.state.modalOpen} close={() => this.setState({modalOpen: false})}/>}
                <div className="splash"><h1>Donate to Classify</h1><h3>Help support the project now and use it free, forever.</h3>
                    <Button bsSize="lg" bsStyle="primary" onClick={() => this.setState({modalOpen: true})}>DONATE NOW</Button>
                </div>
                <div className="center" style={{padding: "85px 0"}}>

                    <h2>Why Donate?</h2>
                    <p>Right now, classify is in a testing phase, which means I'm willing to let a select number of people use it for free, but at some point, I'm going to have to start charging so I can put food on my table.  However, if you make a donation right now, I'll make note, and you can use Classify for free forever!</p>

                    <br style={{paddingBottom: 20}}/>
                    <h2>Who am I donating to?</h2>
                    <Image style={{float: "right", maxWidth: 230, marginLeft: 15}} circle src={eric} alt="Eric Newbury" />
                    <p>My name is Eric Newbury and I started this project during my junior year at UVM after I missed getting into some crucial classes, and wanted something to help me and my friends out.  After graduating I worked for a year for Dealer.com, but this project kept nagging at the back of my mind.  I felt that this could be something useful to students, not just at UVM, but all across the world.  In June 2017, I left my work at Dealer.com and started working on Classify full time.  Life is simple.  I live out of a cardboard box dresser and I don't owe anyone anything, but still need to pay the rent, so your donations are greatly appreciated while I get this off the ground.</p>
                    <div style={{textAlign: "center", margin: 80}}>
                        <Button bsSize="lg" style={{fontWeight: 800}} bsStyle="primary" onClick={() => this.setState({modalOpen: true})}>DONATE NOW</Button>
                    </div>
                </div>
            </div>
        );
    }
}

Donate.propTypes = {};
Donate.defaultProps = {};


export default Donate;