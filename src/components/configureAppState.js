import React, {Component} from 'react';
import {connect} from "react-redux";
import * as courseDataActions from '../actions/courseDataActions';

export const configureAppState = (WrappedComponent) => {
    class AppState extends Component {

        constructor(props){
            super(props);
            this.loadStateWhenAuthenticated = this.loadStateWhenAuthenticated.bind(this);
        }

        loadStateWhenAuthenticated(authenticatedBefore, authenticatedAfter){
            if(authenticatedAfter && authenticatedAfter !== authenticatedBefore) {
                this.props.actions.fetchTerms(() => {
                    let selectedTermId = parseInt(localStorage.getItem("selectedTermId"), 10) || null;
                    if (selectedTermId) {
                        this.props.actions.selectTerm(selectedTermId);
                    }
                });
            }
        }

        componentWillReceiveProps(nextProps){
            this.loadStateWhenAuthenticated(this.props.authenticated, nextProps.authenticated);
        }

        componentDidMount(){
            this.loadStateWhenAuthenticated(null, this.props.authenticated)
        }


        render(){
            return <WrappedComponent {...this.props} />
        }
    }

    const mapStateToProps = (state) => {
        return {authenticated: state.user.authenticated}
    };

    const mapDispatchToProps = (dispatch) => {
        return {
            actions: {
                fetchTerms: (callback) => dispatch(courseDataActions.fetchTerms(callback)),
                selectTerm: (termId) => dispatch(courseDataActions.selectTerm(termId))
            }
        }
    };

    return connect(mapStateToProps, mapDispatchToProps)(AppState);
};
