import React, {Component} from 'react';
import ReactList from "react-list"
import SearchResult from "../layout/SearchResult";
import {FaBinoculars} from "react-icons/lib/fa";
import {connect} from "react-redux";
import {arraysIntersect} from "../../helper/utilities";
import {isAfter} from "../../helper/timeHelpers";
import {Link} from "react-router-dom";
import {SortBy} from "../../helper/constants";
import {FormControl} from "react-bootstrap";

class SearchResults extends Component {
    /** @namespace instructor.rmpRating */
    constructor(props){
        super(props);
        this.state = {sortBy: SortBy.SUBJECT, results: this.doFilter(props)};
        this.renderResult = this.renderResult.bind(this);
        this.doFilter = this.doFilter.bind(this);
        this.sortByChanged = this.sortByChanged.bind(this);

    }

    sortByChanged(value){
        this.setState({sortBy: value, results: this.sort(this.state.results, value)});
    }

    componentWillReceiveProps(props){
        this.setState({results:this.doFilter(props)});
    }

    doFilter(props){
        let results = [];
        Object.keys(props.sections).forEach(sectionId => {
            let section = props.sections[sectionId];
            let course = props.courses[section.courseId];
            let instructor = props.instructors[section.instructorId];

            if(this.matchesFilters(course,section,instructor, props.filters)){
                results.push({course, instructor, section});
            }
        });

        return this.sort(results, (this.state) ? this.state.sortBy : SortBy.SUBJECT);
    }

    sort(results, sortBy){
        return results.sort((a, b) => {
            switch(sortBy){
                case SortBy.SUBJECT:
                    return a.course.subject + a.course.courseNumber + a.section.sectionName < b.course.subject + b.course.courseNumber + b.section.sectionName ? -1 : 1;
                case SortBy.NAME:
                    return a.course.name < b.course.name ? -1 : 1;
                case SortBy.PROFESSOR:
                    if(!a.instructor) return -1;
                    else if (!b.instructor) return 1;
                    else return a.instructor.lastName + a.instructor.firstName < b.instructor.lastName + a.instructor.firstName ? -1 : 1;
                default:
                    return true;
            }
        });
    }

    matchesFilters(course, section, instructor, filters){

        //check term
        if(section.termId !== this.props.termId) return false;

        //check name
        if (filters.title && !course.name.includes(filters.title)) return false;

        //check subject
        if(filters.subject && course.subject !== filters.subject) return false;

        //check course number
        if(filters.courseNumber && !course.courseNumber.includes(filters.courseNumber)) return false;

        //check instructor names
        if(filters.instructors && filters.instructors.length > 0 && filters.instructors.indexOf(section.instructorId) === -1) return false;

        //check instructor rating
        if(filters.minInstructorScore && (!instructor || !instructor.rmpRating || instructor.rmpRating < filters.minInstructorScore)) return false;

        //check show empty time slots
        if(!filters.includeEmptyTime && section.timeSlots.length === 0) return false;

        let timeSlotMatches = true;
        section.timeSlots.forEach(timeSlot => {
            //no matching days
            if(filters.days && filters.days.length > 0 && !arraysIntersect(timeSlot.daysList, filters.days)) {
                timeSlotMatches = false;
                return;
            }
            // time slot out of range
            if(isAfter(filters.after, timeSlot.startTime) || isAfter(timeSlot.endTime, filters.before)) {
                timeSlotMatches = false;
            }
        });

        return timeSlotMatches;
    }

    renderResult(index, key){
        let course = this.state.results[index].course;
        let section = this.state.results[index].section;
        let instructor = this.state.results[index].instructor;

        return <Link style={{color: "inherit", display: "block"}} to="/lists/details" key={key}>
            <SearchResult matchers={{}} course={course} section={section} instructor={instructor} />
        </Link>
    }

    render() {
        return (
            <div>
                <div style={{padding: 15}}>
                    <div className="float-right" style={{paddingTop: 20}}>
                        <span>Sort By: </span>
                        <FormControl bsSize="sm" style={{display: "inline-block", width: "auto"}} componentClass="select" onChange={(e) => this.sortByChanged(e.target.value)}>
                            <option value={SortBy.SUBJECT}>Subject</option>
                            <option value={SortBy.NAME}>Name</option>
                            <option value={SortBy.PROFESSOR}>Professor</option>
                        </FormControl>
                    </div>
                    <h2>Results</h2>
                </div>
                {/* Results */}
                {(this.state.results.length > 0) ?
                    <ReactList
                        itemRenderer={this.renderResult}
                        length={this.state.results.length}
                        type='uniform'
                    />
                    :
                    <div style={{padding: 50}}>
                        <div style={{margin: "30px auto", maxWidth: 500, textAlign: "center"}}>
                            <FaBinoculars style={{height: 45, width: 45, fill: "#888"}}/>
                            <h3 style={{color: "#777"}}>No Matches</h3>
                        </div>
                    </div>
                }
            </div>
        );
    }
}

SearchResults.propTypes = {};
SearchResults.defaultProps = {};

export const mapStateToProps = (state) => {
    return {
        courses: state.courseData.entities.courses,
        instructors: state.courseData.entities.instructors,
        sections: state.courseData.entities.sections,
        filters: state.search,
        termId: state.selectedTermId
    }
};

export default connect(mapStateToProps)(SearchResults);