import React, {Component} from 'react';
import PropTypes from "prop-types"
import {
    Button,
    Clearfix, FormControl, FormGroup, InputGroup, ToggleButton,
    ToggleButtonGroup
} from "react-bootstrap";
import "./SearchPanel.css"
import {connect} from "react-redux";
import Select from "react-select";
import {FaClockO} from "react-icons/lib/fa"
import 'react-select/dist/react-select.css';
import 'rc-slider/assets/index.css';
import {FRI, MON, SAT, SUN, THU, TUE, WED} from "../../helper/constants";
import {Range} from "rc-slider";
import Slider from "rc-slider/es/Slider";
import {hoursToTime, parseTimeString, timeToHours, timeToString} from "../../helper/timeHelpers";
import {filterResults} from "../../actions/searchActions";
import Checkbox from "../layout/Checkbox";

class SearchPanel extends Component {

    constructor(props){
        super(props);
        this.updateTimesFromRange = this.updateTimesFromRange.bind(this);
        this.updateAfterTime = this.updateAfterTime.bind(this);
        this.updateBeforeTime = this.updateBeforeTime.bind(this);
        this.search = this.search.bind(this);
        this.state = props.filters;
    }

    updateTimesFromRange(values){
        this.setState({
            after: hoursToTime(values[0]),
            before: hoursToTime(values[1])
        });
    }

    updateAfterTime(e){
        this.setState({
            after: parseTimeString(e.target.value)
        })
    }

    updateBeforeTime(e){
        this.setState({
            before: parseTimeString(e.target.value)
        })
    }

    search(){
        this.props.actions.doSearch(this.state);
    }

    render() {
        return (
            <div className="SearchPanel clear">
                <h4>Course Info</h4>
                <FormGroup><FormControl placeholder="Course Title" value={this.state.title} onChange={(e) => this.setState({title: e.target.value})}/></FormGroup>
                <FormGroup>
                    <div style={{float: "left", width: "calc(60% - 15px)", marginRight: 15}}>
                        <Select
                            value={this.state.subject}
                            placeholder="Subject"
                            options={this.props.subjects.map(subj => ({label: subj, value: subj}))}
                            onChange={(val) => this.setState({subject: (val || {value: ""}).value})}
                        />
                    </div>
                    <FormControl style={{float: "left", width: "40%"}} placeholder="Course Number" value={this.state.courseNumber} onChange={(e) => this.setState({courseNumber: e.target.value})}/>
                    <Clearfix/>
                </FormGroup>

                <h4>Instructors</h4>
                <FormGroup>
                    <Select
                        value={this.state.instructors}
                        placeholder="Instructors"
                        multi={true}
                        options={this.props.instructors.map(prof => ({label: prof.firstName + " " + prof.lastName, value: prof.id}))}
                        onChange={(values) => this.setState({instructors: values.map(val => val.value)})}
                    />
                </FormGroup>

                <h5>Instructor Rating: <span style={{fontWeight: "normal"}}>{this.state.minInstructorScore} - 5</span></h5>
                <div style={{padding: 10}}>
                    <Slider style={{marginBottom: 25}} value={this.state.minInstructorScore} onChange={(val) => this.setState({minInstructorScore: val})} max={5} step={0.5} marks={{0:0,1:1,2:2,3:3,4:4,5:5}} />
                </div>
                <h4>Times</h4>

                <Checkbox style={{paddingTop: 10}} checked={true} label="Include sections with no meeting time" onClick={(checked) => this.setState({includeEmptyTime: checked})}/>
                <h5 style={{paddingTop: 10}}>Includes Days</h5>
                <ToggleButtonGroup type="checkbox" value={this.state.days} onChange={values => this.setState({days: values})}>
                    <ToggleButton value={SUN} bsStyle="info">Su</ToggleButton>
                    <ToggleButton value={MON} bsStyle="info">Mo</ToggleButton>
                    <ToggleButton value={TUE} bsStyle="info">Tu</ToggleButton>
                    <ToggleButton value={WED} bsStyle="info">We</ToggleButton>
                    <ToggleButton value={THU} bsStyle="info">Th</ToggleButton>
                    <ToggleButton value={FRI} bsStyle="info">Fr</ToggleButton>
                    <ToggleButton value={SAT} bsStyle="info">Sa</ToggleButton>
                </ToggleButtonGroup>

                <div className="time-inputs clear">

                    <InputGroup className="pull-left" bsSize="sm"><InputGroup.Addon><FaClockO /></InputGroup.Addon>
                        <FormControl disabled value={timeToString(this.state.after, false)} onChange={this.updateAfterTime} />
                    </InputGroup>

                    <InputGroup className="pull-right" bsSize="sm">
                        <FormControl disabled value={timeToString(this.state.before, false)} onChange={this.updateBeforeTime} />
                        <InputGroup.Addon><FaClockO /></InputGroup.Addon>
                    </InputGroup>
                </div>
                <div style={{padding: 10}}>
                    <Range value={[timeToHours(this.state.after), timeToHours(this.state.before)]} min={6} max={22} step={1/12} onChange={this.updateTimesFromRange}/>
                </div>

                <Button style={{width: "100%", marginTop: 20}} bsStyle="primary" bsSize="lg" onClick={this.search}>Search Courses</Button>
            </div>
        );
    }
}

SearchPanel.propTypes = {subjects: PropTypes.array};
SearchPanel.defaultProps = {};

const mapStateToProps = (state) => {
    const props = {
        subjects: [],
        instructors: [],
        filters: state.search
    };

    if(state.courseData.isLoaded[state.selectedTermId] === true) {
        //get unique subjects from courses
        props.subjects = [...new Set(Object.values(state.courseData.entities.courses).map(c => c.subject))];
        props.instructors = Object.values(state.courseData.entities.instructors)
    }

    return props;
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            doSearch: (filters) => dispatch(filterResults(filters))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchPanel);