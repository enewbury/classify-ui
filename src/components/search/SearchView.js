import React, {Component} from 'react';

import "./SearchView.css"
import {Col} from "react-bootstrap";
import SearchPanel from "./SearchPanel";

import SearchResults from "./SearchResults";
import AddMoveModal from "../AddMoveModal/AddMoveModal";
import FeedbackPanel from "../FeedbackPanel";

class SearchView extends Component {

    render() {
        return (
            <div className="full-height SearchView">
                <Col sm={4} className="border-right shade-2 full-height-auto">
                    <SearchPanel/>
                </Col>
                <Col sm={8} className="no-padding full-height-auto">
                    <SearchResults />
                </Col>
                <AddMoveModal />
                <div style={{position: "fixed", bottom: 0, right: 15, zIndex: 4}}><FeedbackPanel/></div>
            </div>
        );
    }
}

SearchView.propTypes = {};
SearchView.defaultProps = {};



export default SearchView;