import React, {Component} from 'react';
import eric from "./face.jpg"
import "./About.css"
import {Button, Col, Image, Row} from "react-bootstrap";

class About extends Component {


    render() {
        return (
            <div className="About full-height-auto">
                <div className="splash"><h1>My Story</h1>
                </div>
                <div className="center" style={{padding: "85px 0"}}>
                    <h1>Who's Eric Newbury?</h1>
                    <Row>
                        <Col sm={8}>
                            <p>My name is Eric Newbury and I started this project during my junior year at UVM after I missed getting into some crucial classes. I wanted something to help me and my friends out.  After graduating I worked for a year for Dealer.com, but this project kept nagging at the back of my mind.  I felt that this could be something useful to students, not just at UVM, but all across the world.</p>
                            <p>In June 2017, I left my work at Dealer.com, moved to Washington DC and started working on Classify full time while pursuing my competitive goals with a new ballroom dance partner.  Life is simple.  I live out of a cardboard box dresser, but I like the simple life, training, programming, and hoping that this will grow to a product I can offer to universities around the world.</p>
                        </Col>
                        <Col sm={4}>
                            <Image style={{maxWidth: "100%", marginTop: 20}} circle src={eric} alt="Eric Newbury" />
                        </Col>
                    </Row>
                        <div style={{textAlign: "center", margin: 80}}>
                        <Button bsSize="lg"  bsStyle="primary" href="https://www.ericnewbury.com">Check Out My Website</Button>
                    </div>
                </div>
            </div>
        );
    }
}

About.propTypes = {};
About.defaultProps = {};


export default About;