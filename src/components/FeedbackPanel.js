import React, {Component} from 'react';
import {Button, FormControl, FormGroup, Modal, Panel} from "react-bootstrap";
import {API, SERVER} from "../helper/constants";

class FeedbackPanel extends Component {

    constructor(props){
        super(props);
        this.state = {showModal: false, inputValue: "", processing: false, status: null, message: ""};
        this.submitFeedback = this.submitFeedback.bind(this);
    }

    submitFeedback(){
        this.setState({processing: true});
        fetch(SERVER + API + "/feedback", {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            credentials: 'include',
            body: JSON.stringify({feedback: this.state.inputValue})
        }).then(
            response => {
                if (response.ok) {
                    this.setState({processing: false, status: "ok", message: "Thank You! We successfully sent your feedback."})
                } else {
                    response.json().then(json => {
                        this.setState({processing: false, status: "failed", message: json.message});
                    });
                }
            },
            errorResponse => {
                this.setState({processing: false, status: "failed", message: "Unfortunately we couldn't connect to the server.  Try emailing me your feedback at enewbury94@icloud.com and let me know my feedback form is down :)"});
            }
        );
    }

    render() {
        let buttonStyle = {
            border: "solid 2px #ccc",
            borderBottom: 0,
            borderTopRightRadius: 5,
            borderTopLeftRadius: 5,
            display: "inline-block",
            padding: "8px 15px",
            backgroundColor: "#e5e5e5",
            cursor: "pointer"
        };
        return (
            <span style={buttonStyle} onClick={() => this.setState({showModal: true})}>
                Submit Feedback
                <Modal show={this.state.showModal} onHide={()=> this.setState({showModal: false})}>
                    <Modal.Header closeButton><Modal.Title>Submit Feedback</Modal.Title></Modal.Header>
                    <Modal.Body>
                        <p>Submit ideas, or report a problem.  If reporting a problem, please include any details you can remember about what you were doing with the problem occurred.</p>
                        {this.state.status !== null && <Panel bsStyle={(this.state.status === "ok") ? "info" : "danger"}>{this.state.message}</Panel>}
                        <FormGroup>
                          <FormControl style={{minHeight: 200}} componentClass="textarea" placeholder="Feedback..." value={this.state.inputValue} onChange={(e) => this.setState({inputValue: e.target.value})}/>
                        </FormGroup>
                    </Modal.Body>
                    <Modal.Footer><Button disabled={this.state.processing} onClick={this.submitFeedback} bsStyle="primary">Submit</Button></Modal.Footer>
                </Modal>
            </span>
        );
    }
}

FeedbackPanel.propTypes = {};
FeedbackPanel.defaultProps = {};

export default FeedbackPanel;