import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {FormControl, FormGroup, ListGroup, ListGroupItem, Modal, Panel} from "react-bootstrap";
import * as userActions from "../../../actions/userActions"
import {connect} from "react-redux";
import {API, SERVER} from "../../../helper/constants";

class AccountModal extends Component {

    constructor(props){
        super(props);
        this.state = {
            editingName: false,
            editingEmail: false,
            editingPassword: false,
            valid: true,
            validationMessage: "",
            firstNameValue: props.user.firstName,
            lastNameValue: props.user.lastName,
            emailValue: props.user.email,
            emailPasswordValue: "",
            passwordValue: "",
            confirmValue: ""
        };

        this.updateFirstName = this.updateFirstName.bind(this);
        this.updateLastName = this.updateLastName.bind(this);
        this.updateEmail = this.updateEmail.bind(this);
        this.updateEmailPassword = this.updateEmailPassword.bind(this);
        this.updatePassword = this.updatePassword.bind(this);
        this.updateConfirm = this.updateConfirm.bind(this);
        this.saveName = this.saveName.bind(this);
        this.saveEmail = this.saveEmail.bind(this);
        this.savePassword = this.savePassword.bind(this);
    }

    updateFirstName(e){
        this.setState({firstNameValue: e.target.value});
    }

    updateLastName(e){
        this.setState({lastNameValue: e.target.value});
    }

    updateEmail(e){
        this.setState({emailValue: e.target.value});
    }

    updateEmailPassword(e){
        this.setState({emailPasswordValue: e.target.value});
    }

    updatePassword(e) {
        this.setState({passwordValue: e.target.value});
    }


    updateConfirm(e){
        this.setState({confirmValue: e.target.value});
    }

    saveName(){
        this.props.actions.updateName(this.state.firstNameValue, this.state.lastNameValue);
        this.setState({editingName: false});
    }

    saveEmail(){
        fetch(SERVER + API + "/user/me?type=EMAIL", {
            method: "PATCH",
            headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
            body: JSON.stringify({email: this.state.emailValue, password: this.state.emailPasswordValue}),
            credentials: 'include'
        }).then(
            response => {
                if (response.ok) {
                    this.setState({updatingEmail: false, valid: true, validationMessage: "Verification email sent to the new email address. Check your email to finish updating the account email."});
                } else {
                    response.json().then(json => {
                        this.setState({valid: false, validationMessage: json.message});
                    });
                }
            },
            errorResponse => {
                this.setState({valid: false, validationMessage: "There was an error connecting to the server"});
            }
        );

    }

    savePassword(){
        if(this.state.passwordValue.length < 6){
            this.setState({valid: false, validationMessage: "Password must be at least 6 characters"});
        } else if (this.state.passwordValue !== this.state.confirmValue){
            this.setState({valid: false, validationMessage: "Passwords do not match."});
        } else {
            this.props.actions.updatePassword(this.state.passwordValue, this.state.confirmValue);
            this.setState({editingPassword: false});
        }
    }

    render() {
        let inputStyle = {
            width: 200,
            display: "inline-block"
        };

        return (
            <Modal show={this.props.show} onHide={this.props.onHide}>
                <Modal.Header closeButton>
                    <Modal.Title>Account Options</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <ListGroup>
                        {!this.state.valid && <Panel bsStyle="danger">{this.state.validationMessage}</Panel> }
                        {this.state.valid && this.state.validationMessage && <Panel bsStyle="info">{this.state.validationMessage}</Panel> }
                        <ListGroupItem>
                            <h5>Name</h5>
                            <a onClick={() => this.setState({editingName: !this.state.editingName})} className="float-right">{(this.state.editingName) ? "Cancel" : "Change"}</a>
                            {this.state.editingName && <a style={{marginRight: 7}} onClick={this.saveName} className="float-right">Save</a>}
                            {(this.state.editingName) ?
                                <FormGroup>
                                    <FormControl value={this.state.firstNameValue} onChange={this.updateFirstName} style={{...inputStyle, marginRight: 10}} bsSize="sm" />
                                    <FormControl value={this.state.lastNameValue} onChange={this.updateLastName} style={inputStyle} bsSize="sm" />
                                </FormGroup>
                            :
                                <span>{this.props.user.firstName + " " + this.props.user.lastName}</span>
                            }
                        </ListGroupItem>
                        <ListGroupItem>
                            <h5>Email</h5>
                            <a onClick={() => this.setState({editingEmail: !this.state.editingEmail})} className="float-right">{(this.state.editingEmail) ? "Cancel" : "Change"}</a>
                            {this.state.editingEmail && <a style={{marginRight: 7}} onClick={this.saveEmail} className="float-right">Save</a>}
                            {(this.state.editingEmail) ?
                                <FormGroup>
                                    <FormControl style={{...inputStyle, marginRight: 10}} value={this.state.emailValue} onChange={this.updateEmail} bsSize="sm" />
                                    <FormControl style={{...inputStyle}} type="password" value={this.state.emailPasswordValue} onChange={this.updateEmailPassword} bsSize="sm" />
                                </FormGroup>
                            :
                                <span>{this.props.user.email}</span>
                            }

                        </ListGroupItem>
                        <ListGroupItem>
                            <h5>Password</h5>
                            <a onClick={() => this.setState({editingPassword: !this.state.editingPassword})}className="float-right">{(this.state.editingPassword) ? "Cancel" : "Update"}</a>
                            {this.state.editingPassword && <a style={{marginRight: 7}} onClick={this.savePassword} className="float-right">Save</a>}
                            {(this.state.editingPassword) ?
                                <FormGroup>
                                    <FormControl type="password" bsSize="sm" style={{...inputStyle, marginRight: 10}} value={this.state.passwordValue} onChange={this.updatePassword} placeholder="Password"/>
                                    <FormControl type="password" bsSize="sm" style={inputStyle} placeholder="Confirm" value={this.state.confirmValue} onChange={this.updateConfirm} />
                                </FormGroup>
                            :
                                <span>******</span>
                            }

                        </ListGroupItem>
                    </ListGroup>
                </Modal.Body>
            </Modal>
        );
    }
}

AccountModal.propTypes = {show: PropTypes.bool, onHide: PropTypes.func};
AccountModal.defaultProps = {};

const mapStateToProps = (state) => {
    return {
        user: state.user.user
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            updateName: (firstName, lastName) => dispatch(userActions.updateName(firstName, lastName)),
            updatePassword: (password) => dispatch(userActions.updatePassword(password))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AccountModal);