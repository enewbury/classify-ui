import React, { Component } from 'react';
import { connect } from "react-redux";
import {LinkContainer} from "react-router-bootstrap"
import {Navbar, Nav, NavItem} from 'react-bootstrap';
import LoadingBar from "react-redux-loading-bar";
import PropTypes from "prop-types"
import logo from "./logo.png";
import "./Header.css"
import {Link, withRouter} from "react-router-dom";
import AuthenticatedNav from "./AuthenticatedNav";

class Header extends Component {
    render(){

        return (
            <header className="Header">
                <LoadingBar style={{zIndex: 3, backgroundColor: "#00b06c"}}/>
                <Navbar collapseOnSelect inverse>
                    <Navbar.Header>
                        <Navbar.Brand>
                            <Link to="/">
                                <img className="Header-logo" src={logo} alt="Classify Logo"/>
                                <span className="Header-logo-text">Classify</span>
                            </Link>
                        </Navbar.Brand>
                        <Navbar.Toggle />
                    </Navbar.Header>
                    <Navbar.Collapse>

                        {this.props.authenticated &&
                            <AuthenticatedNav />
                        }
                        {!this.props.authenticated &&
                            <Nav pullRight>
                                <LinkContainer to="/about"><NavItem eventKey={1}>About</NavItem></LinkContainer>
                                {/*<LinkContainer to="/donate">*/}
                                    {/*<NavItem eventKey={0}>Donate</NavItem>*/}
                                {/*</LinkContainer>*/}
                                <LinkContainer to="/login">
                                    <NavItem eventKey={1}>Login</NavItem>
                                </LinkContainer>
                            </Nav>
                        }
                    </Navbar.Collapse>
                </Navbar>
            </header>
        );
    }
}

Header.propTypes = {
    authenticated: PropTypes.bool,
};

function mapStateToProps(state) {
    return {
        authenticated: state.user.authenticated,
        user: state.user.user,
        termsLoaded: state.terms.isLoaded,
        terms: state.terms,
        selectedTermId: state.selectedTermId,
        courseDataLoaded: state.courseData.isLoaded[state.selectedTermId] === true,
        courseData: state.courseData.entities,
        watchList: state.watchList.entries
    };
}


export default withRouter(connect(mapStateToProps)(Header));
