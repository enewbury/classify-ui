import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {LinkContainer} from "react-router-bootstrap";
import {FaBell, FaClose} from "react-icons/lib/fa"
import ColorHash from "color-hash"
import {Badge, Button, MenuItem, Nav, NavDropdown, NavItem, Overlay, OverlayTrigger, Popover} from "react-bootstrap";
import {connect} from "react-redux";
import * as courseDataActions from "../../../actions/courseDataActions";
import {removeFromWatchListBySection} from "../../../actions/watchListActions";
import {selectSection} from "../../../actions/listsActions";
import {logout} from "../../../actions/userActions";
import AccountModal from "./AccountModal";
import {withRouter} from "react-router-dom";

class LoggedInHeader extends Component {
    /** @namespace this.props.user.lastName */
    /** @namespace this.props.user.firstName */
    /** @namespace this.props.actions.selectTerm */
    /** @namespace term.year */
    /** @namespace term.semester */

    constructor(props){
        super(props);
        this.state = {accountWindowOpen: false};
        this.getWatchListPopoverContent = this.getWatchListPopoverContent.bind(this);
        this.viewSectionInTabsView = this.viewSectionInTabsView.bind(this);
    }

    getWatchListPopoverContent(){
        let colorHash = new ColorHash({lightness: 0.55, saturation: 0.8});
        if (Object.keys(this.props.watchList).length > 0 && this.props.courseDataLoaded) {
            return <ul>
                {Object.values(this.props.watchList).map(entry => {
                    let section = this.props.courseData.sections[entry.sectionId];
                    let course = this.props.courseData.courses[section.courseId];
                    let filledColor = (section.seatsFilled/section.seats < 0.9) ? "text-info" : "text-danger";
                    return <li key={entry.id} onClick={() => this.viewSectionInTabsView(entry.sectionId)}>
                        <span style={{color: colorHash.hex(course.subject)}}>{course.subject}</span>&nbsp;
                        <span>{course.courseNumber}</span>&nbsp;-&nbsp;
                        <span>{section.sectionName}</span>
                        <span className="float-right"><FaClose className="stop-watching-btn" onClick={() => this.props.actions.removeWatchlistEntry(this.props.selectedTermId, entry.sectionId) }/></span>
                        <span className={"float-right " + filledColor}>{section.seatsFilled}/{section.seats}</span>
                    </li>
                })}
            </ul>
        } else {
            return <div>No Entries</div>
        }
    }

    viewSectionInTabsView(sectionId){
        this.props.actions.viewSection(sectionId, this.props.selectedTermId);
    }

    render() {
        let termId = this.props.selectedTermId;
        let terms = this.props.terms;
        return (
            <div>
                <Nav>
                    <LinkContainer to="/lists"><NavItem eventKey={1}>Schedules</NavItem></LinkContainer>
                    <LinkContainer to="/search"><NavItem eventKey={2}>Advanced Search</NavItem></LinkContainer>
                </Nav>

                <Nav pullRight>
                    <NavItem eventKey={0}>
                        <OverlayTrigger trigger="click" rootClose placement="bottom" overlay={
                            <Popover title={<b>Watchlist</b>} id="watchlistDropdownPopover">{this.getWatchListPopoverContent()}</Popover>
                        }>
                            <Button bsStyle="primary" bsSize="sm"><FaBell /> <Badge>{Object.keys(this.props.watchList).length}</Badge></Button>
                        </OverlayTrigger>
                    </NavItem>
                    <NavDropdown eventKey={1}
                                 title={termId ? (terms.entities[termId].semester + " " + terms.entities[termId].year) : "Select Term"}
                                 id="bg-term-select"
                                 ref={(node) => this.termDropdown = node}
                    >
                        {this.props.termsLoaded && this.props.terms.result.length > 0 &&
                        terms.result.map(id =>
                            <MenuItem key={id} eventKey={id} onSelect={this.props.actions.selectTerm}>
                                {terms.entities[id].semester + " " + terms.entities[id].year}
                            </MenuItem>
                        )}
                    </NavDropdown>
                    <Overlay show={this.props.termsLoaded && !this.props.selectedTermId} target={this.termDropdown} placement="left">
                        <Popover id="select-term-prompt-popover">Select a term to get started.</Popover>
                    </Overlay>

                    <NavDropdown eventKey={2} title={(this.props.user.firstName) ? this.props.user.firstName + " " + this.props.user.lastName : this.props.user.email} id="basic-nav-dropdown">
                        <MenuItem onClick={() => this.setState({accountWindowOpen: true})} eventKey={2.1}>Account Settings</MenuItem>
                        {/*<LinkContainer to="/donate"><MenuItem eventKey={2.2}>Donate</MenuItem></LinkContainer>*/}
                        <MenuItem eventKey={2.3} onClick={this.props.actions.logout}>Logout</MenuItem>
                    </NavDropdown>
                    <AccountModal show={this.state.accountWindowOpen} onHide={() => this.setState({accountWindowOpen: false})} />
                </Nav>
            </div>
        );
    }
}

LoggedInHeader.propTypes = {
    user: PropTypes.object,
    terms: PropTypes.object.isRequired,
    termsLoaded: PropTypes.bool.isRequired,
    actions: PropTypes.object.isRequired
};

LoggedInHeader.defaultProps = {};

function mapStateToProps(state) {
    return {
        user: state.user.user,
        termsLoaded: state.terms.isLoaded,
        terms: state.terms,
        selectedTermId: state.selectedTermId,
        courseDataLoaded: state.courseData.isLoaded[state.selectedTermId] === true,
        courseData: state.courseData.entities,
        watchList: state.watchList.entries
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: {
            selectTerm: (termId) => dispatch(courseDataActions.selectTerm(termId)),
            viewSection: (sectionId, termId) => dispatch(selectSection(sectionId, termId)),
            logout: () => dispatch(logout()),
            removeWatchlistEntry: (termId, sectionId) => dispatch(removeFromWatchListBySection(termId, sectionId))
        }
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LoggedInHeader));