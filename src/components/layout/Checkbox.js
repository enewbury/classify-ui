import React, {Component} from 'react';
import PropTypes from 'prop-types';

class Checkbox extends Component {
    constructor(props){
        super(props);
        this.clicked = this.clicked.bind(this);
        this.state = {checked: props.checked || false}
    }

    clicked(){
        if(this.props.onClick){
            this.props.onClick(!this.state.checked);
        }
        this.setState({checked: !this.state.checked})
    }

    render() {
        let outerStyle = {
            display: "inline-block",
            width: 18,
            height: 18,
            border: "solid 1px #bbb",
            borderRadius: 3,
            verticalAlign: "middle"
        };

        let innerStyle = {
            margin: 2,
            width: 12,
            height: 12,
            backgroundColor: "#00b06c",
            borderRadius: 3
        };

        return (
            <div style={this.props.style} onClick={this.clicked}>
                <div style={outerStyle}>
                    {this.state.checked && <div style={innerStyle} />}
                </div>
                {this.props.label && <span style={{marginLeft: 7}}>{this.props.label}</span>}
            </div>
        );
    }
}

Checkbox.propTypes = {label: PropTypes.string, onClick: PropTypes.func, style: PropTypes.object};
Checkbox.defaultProps = {};

export default Checkbox;