import React, {Component} from 'react';
import PropTypes from 'prop-types';
import clamp from "clamp-js";

class Clamp extends Component {

    componentDidMount(){
        if (this.container.length) {
            throw new Error('Please provide exactly one child to clamp');
        }

        clamp(this.container, {
            clamp: this.props.clamp,
            truncationChar: this.props.truncationChar,
            useNativeClamp: this.props.useNativeClamp
        });
    }

    render() {
        return (
            <div ref={(div) => this.container = div}>{this.props.children}</div>
        );
    }
}

Clamp.propTypes = {
    clamp: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.bool
    ]),
    truncationChar: PropTypes.string,
    className: PropTypes.string,
    useNativeClamp: PropTypes.bool
};
Clamp.defaultProps = {
    clamp: 2,
    truncationChar: '\u2026',
    useNativeClamp: true
};

export default Clamp;