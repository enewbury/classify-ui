import React, {Component} from 'react';
import Radiator from "../Radiator/Radiator"
import {Button, Col, OverlayTrigger, Tooltip} from "react-bootstrap"
import { FaPlus } from "react-icons/lib/fa"
import Clamp from "./Clamp"
import {convertToShortWeekday, timeToString} from "../../helper/timeHelpers"
import PropTypes from 'prop-types';
import Highlight from "react-highlighter"
import {connect} from "react-redux";
import * as scheduleActions from "../../actions/listsActions";
import {isContainerEntry, getScheduleIdsForSection} from "../../helper/entryHelpers";
import {FaBell} from "react-icons/lib/fa/index";
import {addToWatchList, removeFromWatchListBySection} from "../../actions/watchListActions";
import {hideAddMoveModal, showAddModal} from "../../actions/addMoveModalActions";

class SearchResult extends Component {

    constructor(props){
        super(props);
        this.state = {watching: props.watching};
        this.addSectionToSchedule = this.addSectionToSchedule.bind(this);
        this.showPopover = this.showPopover.bind(this);
        this.hidePopover = this.hidePopover.bind(this);
        this.toggleWatching = this.toggleWatching.bind(this);
    }

    showPopover(e){
        e.stopPropagation();
        e.preventDefault();
        this.props.actions.showAddModal(this.props.section.id, this.props.sectionScheduleIds);
    }

    hidePopover(){
        this.props.actions.hideAddMoveModal();
    }

    addSectionToSchedule(){
        let parentId = (isContainerEntry(this.props.selectedListEntry.type)) ? this.props.selectedListEntry.id : this.props.selectedListEntry.parentId;
        this.props.actions.addToSelectedList(this.props.selectedTermId, parentId, this.props.section.id);
    }

    formatTimeStamp(timeSlots){
        if (timeSlots.length > 0){
            return timeSlots.map((ts, i) => {
                let days = ts.daysList.map(d => convertToShortWeekday(d)).join("");
                let formattedTs = days + " " + timeToString(ts.startTime) + "-" + timeToString(ts.endTime);
                return <div key={i}>{(timeSlots.length < i-1) ? formattedTs + <br /> : formattedTs}</div>;
            })

        } else {
            return "N/A";
        }
    }

    toggleWatching(e){
        e.stopPropagation();
        e.preventDefault();
        if(!this.state.watching === true){
            this.props.actions.addToWatchList(this.props.selectedTermId, this.props.section.id);
        } else {
            this.props.actions.removeWatchlistEntry(this.props.selectedTermId, this.props.section.id);
        }
        this.setState({watching: !this.state.watching});

    }

    render() {
        let course = this.props.course;
        let section = this.props.section;
        let instructor = this.props.instructor;

        return <div style={{padding: 15}}>
            <div className="clear result" onClick={(e) => this.props.actions.selectSection(section.id, this.props.termId)}>
                <Col sm={1} style={{textAlign: "center"}}>
                    <span style={{display: "inline-block", width: 45, height: 45}}>
                        <Radiator
                            stroke={8}
                            animation={false}
                            value={section.seatsFilled}
                            maxValue={section.seats}
                            initialAnimation={false} />

                    </span>
                    {/*<br /><span>{section.seatsFilled+"/"+section.seats}</span>*/}
                </Col>
                <Col sm={2} style={{textAlign: "center"}}>
                    <Highlight search={this.props.matchers.nameMatcher || ""}>{course.subject + " " + course.courseNumber}</Highlight><br />
                    <span className="text-info">{section.sectionName}</span>
                </Col>
                <Col sm={2}>
                    <Clamp clamp={2}>
                        <Highlight title={course.name} search={this.props.matchers.nameMatcher || ""}>{course.name}</Highlight>
                    </Clamp>

                </Col>
                <Col sm={2}>
                    <Clamp clamp={2}>
                        <Highlight title={instructor && instructor.firstName + " " + instructor.lastName} search={this.props.matchers.instructorMatcher || ""}>{instructor && instructor.firstName + " " + instructor.lastName}</Highlight>
                    </Clamp>
                </Col>
                <Col sm={1}>
                    <small>Credits</small><br />
                    <span className="text-info">{course.creditsMin}{course.creditsMax && "-" + course.creditsMax}</span>
                </Col>

                <Col sm={2}>
                    <Clamp clamp={2}><small>{this.formatTimeStamp(section.timeSlots)}</small></Clamp>
                </Col>
                <Col sm={2} style={{textAlign: "right", marginTop: 5}}>
                    {this.props.watchListLoaded &&
                    <OverlayTrigger placement="left" overlay={<Tooltip id="sendMeEmailUpdatesTooltip">Send me email updates</Tooltip>}>
                        <Button onClick={this.toggleWatching} active={this.state.watching} bsSize="sm" bsStyle={(this.state.watching) ? "info" : "default"}
                                style={{marginRight: 7}}><FaBell/></Button>
                    </OverlayTrigger>
                    }
                    <Button onClick={this.showPopover} bsSize="sm" bsStyle="primary"><FaPlus /></Button>
                </Col>
            </div>
        </div>
    }
}

SearchResult.propTypes = {matchers: PropTypes.object.isRequired,
    section: PropTypes.object.isRequired,
    course: PropTypes.object.isRequired,
    instructor: PropTypes.object
};
SearchResult.defaultProps = {instructor: null};

const mapStateToProps = (state, currentProps) => {
    return {
        termId: state.selectedTermId,
        selectedListEntry: state.lists.entries[state.lists.selectedEntries[state.selectedTermId]] || null,
        selectedTermId: state.selectedTermId,
        sectionScheduleIds: getScheduleIdsForSection(currentProps.section.id, state.selectedTermId, state.lists.entries),
        watchListLoaded: state.watchList.isLoaded,
        watching: state.watchList.isLoaded && (state.watchList.entries[currentProps.section.id] || null) !== null,

    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            showAddModal: (sectionId, disabledScheduleIds) => dispatch(showAddModal(sectionId, disabledScheduleIds)),
            hideAddMoveModal: () => dispatch(hideAddMoveModal()),
            selectSection: (sectionId, termId) => dispatch(scheduleActions.selectSection(sectionId, termId)),
            addToSelectedList: (termId, parentId, sectionId) => dispatch(scheduleActions.addToSelectedList(termId, parentId, sectionId)),
            addToWatchList: (termId, sectionId) => dispatch(addToWatchList(termId, sectionId)),
            removeWatchlistEntry: (termId, sectionId) => dispatch(removeFromWatchListBySection(termId, sectionId))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchResult);