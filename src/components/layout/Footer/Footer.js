import React, {Component} from 'react';
import "./Footer.css"

class Footer extends Component {
    render() {
        return (
            <div className="Footer">
                <div className="center">
                    <span>© classifyregistration.com {(new Date()).getFullYear()}</span>
                    <span className="info"><a href="https://www.ericnewbury.com">Eric Newbury</a> | <a href="mailto:enewbury94@icloud.com">enewbury94@icloud.com</a> | <a href="https://www.facebook.com/classifyRegistration/">Like us on Facebook</a></span>
                </div>
            </div>
        );
    }
}

Footer.propTypes = {};
Footer.defaultProps = {};

export default Footer;