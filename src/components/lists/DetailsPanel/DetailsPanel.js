import React, { Component } from 'react'
import { FaPlus, FaBell } from "react-icons/lib/fa/"
import InstructorPanel from "./InstructorPanel"
import TimeSlotsDisplay from "./TimeSlotsDisplay"
import DetailsHUDBar from "./DetailsHUDBar"
import "./DetailsPanel.css"
import {
    Col, SplitButton, MenuItem, Panel, Tooltip, OverlayTrigger, Button
} from 'react-bootstrap'
import {connect} from "react-redux";
import PropTypes from "prop-types"
import * as scheduleActions from "../../../actions/listsActions"
import {isContainerEntry, getScheduleIdsForSection, getRootForEntry} from "../../../helper/entryHelpers";
import {addToWatchList, removeFromWatchListBySection} from "../../../actions/watchListActions";
import {hideAddMoveModal, showAddModal} from "../../../actions/addMoveModalActions";
import EnrollmentGraph from "./EnrollmentGraph";

class DetailsPanel extends Component {
    /** @namespace this.props.course.description */

    constructor(props){
        super(props);
        this.state = {watching: props.watching};
        this.addSectionToSchedule = this.addSectionToSchedule.bind(this);
        this.toggleWatching = this.toggleWatching.bind(this);
        this.showPopover = this.showPopover.bind(this);
        this.hidePopover = this.hidePopover.bind(this);
    }


    componentWillReceiveProps(nextProps){
        this.setState({watching: nextProps.watching});
    }

    showPopover(){
        this.props.actions.showAddModal(this.props.sectionId, this.props.sectionScheduleIds);
    }

    hidePopover(){
        this.props.actions.hideAddMoveModal();
    }

    toggleWatching(e){
        if(!this.state.watching === true){
            this.props.actions.addToWatchList(this.props.selectedTermId, this.props.sectionId);
        } else {
            this.props.actions.removeWatchlistEntry(this.props.selectedTermId, this.props.sectionId);
        }
        this.setState({watching: !this.state.watching});

    }

    addSectionToSchedule(){
        let parentId = (isContainerEntry(this.props.selectedListEntry.type)) ? this.props.selectedListEntry.id : this.props.selectedListEntry.parentId;
        this.props.actions.addToSelectedList(this.props.selectedTermId, parentId, this.props.sectionId);
    }


    render() {
        let addToListTooltip = <Tooltip id="addToListTooltip">Add section to selected list</Tooltip>;

        return (
            <div className="DetailsPanel clear">
                <DetailsHUDBar section={this.props.section} course={this.props.course} isLoaded={this.props.isLoaded && !this.props.error}/>

                {this.props.isLoaded && !this.props.error &&
                    <div className="base-bg-color clear relative" style={{padding: 20}}>
                        <div className="floating-button">
                            <div>
                                {this.props.watchListLoaded &&
                                <OverlayTrigger placement="left" overlay={<Tooltip id="sendMeEmailUpdatesTooltip">Send me email updates</Tooltip>}>
                                    <Button onClick={this.toggleWatching} active={this.state.watching} bsStyle={(this.state.watching) ? "info" : "default"}
                                            style={{marginRight: 15}}><FaBell/></Button>
                                </OverlayTrigger>
                                }

                                <OverlayTrigger placement="left" overlay={addToListTooltip}>
                                    <SplitButton id="add-button" title={<FaPlus />}
                                         onClick={(this.props.selectedSchedule &&
                                         this.props.sectionScheduleIds.includes(this.props.selectedSchedule.id)) ? this.showPopover : this.addSectionToSchedule}
                                         bsStyle="primary">
                                        <MenuItem eventKey={1} onClick={this.showPopover}>Add To Specific Schedule</MenuItem>
                                    </SplitButton>
                                </OverlayTrigger>
                            </div>
                        </div>

                        <Col sm={6}>
                            <Panel className="big-space-above">
                                <h3>{this.props.course.name}</h3>
                                <p>{this.props.course.description}</p>
                            </Panel>
                            <TimeSlotsDisplay section={this.props.section} course={this.props.course}/>

                        </Col>
                        <Col sm={6}>
                            <Panel className="big-space-above">
                                <EnrollmentGraph section={this.props.section} />
                            </Panel>
                            <InstructorPanel instructor={this.props.instructor}/>
                        </Col>
                    </div>
                }
            </div>
        );
    }
}

DetailsPanel.propTypes = {
    sectionId: PropTypes.number,
    isLoaded: PropTypes.bool,
    selectedScheduleId: PropTypes.number,
    selectedListEntry: PropTypes.object,
    watching: PropTypes.bool
};
DetailsPanel.defaultProps = {sectionId: null};


const mapStateToProps = (state, currentProps) => {
    if(currentProps.sectionId !== null && state.courseData.isLoaded[state.selectedTermId] === true){
        try {
            let section = state.courseData.entities.sections[currentProps.sectionId];
            let course = state.courseData.entities.courses[section.courseId];
            let instructor = state.courseData.entities.instructors[section.instructorId];
            let sectionScheduleIds = getScheduleIdsForSection(currentProps.sectionId, state.selectedTermId, state.lists.entries);
            let selectedEntry = state.lists.entries[state.lists.selectedEntries[state.selectedTermId]] || null;
            return {
                isLoaded: true,
                selectedTermId: state.selectedTermId,
                selectedListEntry: selectedEntry,
                selectedSchedule: getRootForEntry(selectedEntry, state.lists.entries),
                watchListLoaded: state.watchList.isLoaded,
                watching: state.watchList.isLoaded && (state.watchList.entries[currentProps.sectionId] || null) !== null,

                section, course, instructor, sectionScheduleIds
            }
        } catch (error){
            console.log(error);
            return {isLoaded: true, error: true}
        }

    } else {
        return {isLoaded: false}
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            showAddModal: (sectionId, disabledScheduleIds) => dispatch(showAddModal(sectionId, disabledScheduleIds)),
            hideAddMoveModal: () => dispatch(hideAddMoveModal()),
            addToSelectedList: (termId, parentId, sectionId) => dispatch(scheduleActions.addToSelectedList(termId, parentId, sectionId)),
            addToWatchList: (termId, sectionId) => dispatch(addToWatchList(termId, sectionId)),
            removeWatchlistEntry: (termId, sectionId) => dispatch(removeFromWatchListBySection(termId, sectionId))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps) (DetailsPanel);
