//noinspection JSUnresolvedVariable
import React, {Component} from 'react';
import {Col, OverlayTrigger, Tooltip} from "react-bootstrap"
import { FaFlask, FaGlobe, FaQuestion } from "react-icons/lib/fa"
import { GoPodium } from "react-icons/lib/go"
import Radiator from "../../Radiator/Radiator"
import PropTypes from 'prop-types';

class DetailsHUDBar extends Component {
    /** @namespace this.props.course.creditsMin */
    /** @namespace this.props.course.creditsMax */
    /** @namespace this.props.section.seats */
    /** @namespace this.props.section.seatsFilled */

    getIconFromType(type){
        switch (type){
            case "LECTURE":
                return <GoPodium style={{height: 50, width: 50}} />;
            case "LAB":
                return <FaFlask style={{height: 50, width: 50}}/>;
            case "ONLINE":
                return <FaGlobe style={{height: 50, width: 50}}/>;
            default:
                return <FaQuestion style={{height: 50, width: 50}}/>
        }
    }

    render() {
        return (
            <div style={{padding: 30}} className="hud-bar clear">
                {this.props.isLoaded &&
                    <div>
                        <Col sm={2}>
                            <span className="faded-text"><b>{this.props.course.subject}</b>&nbsp;<b>{this.props.course.courseNumber}</b></span><br />
                            <span className="text-info" style={{display: "inline-block", fontSize: 50, lineHeight: "70px", fontWeight: 200}}>{this.props.section.sectionName}</span>
                        </Col>

                        <Col sm={2}>
                            <span className="faded-text"><b>Credits</b></span><br />
                            <span className="text-info" style={{display: "inline-block", fontSize: 50, lineHeight: "70px", fontWeight: 200}}>
                                    {this.props.course.creditsMin}{this.props.course.creditsMax && "-" + this.props.course.creditsMax}
                                </span>
                        </Col>

                        <Col sm={2} className="border-right">
                            <span className="faded-text"><b>Format</b></span><br />
                            <div className="text-info" style={{marginTop: 12}}>
                                <OverlayTrigger placement="bottom" overlay={<Tooltip id="sectionTypeTooltip" style={{textTransform: "capitalize"}}>{this.props.section.type.toLowerCase()}</Tooltip>}>
                                    {this.getIconFromType(this.props.section.type)}
                                </OverlayTrigger>
                            </div>
                        </Col>

                        <Col sm={6}>
                            <Col sm={6}><span style={{display: "inline-block", width: 90, height: 90}}><Radiator percentage={this.props.section.seatsFilled / this.props.section.seats * 100} /></span></Col>
                            <Col sm={3}><small>Seats Filled</small><br /><span className="light-text text-info">{this.props.section.seatsFilled}</span></Col>
                            <Col sm={3}><small>Total Seats</small><br /><span className="light-text text-info">{this.props.section.seats}</span></Col>
                        </Col>

                        <div>
                            <div className="inline-block">
                            </div>
                        </div>
                    </div>
                }
            </div>
        );
    }
}

DetailsHUDBar.propTypes = {course: PropTypes.object.isRequired, section: PropTypes.object.isRequired, isLoaded: PropTypes.bool.isRequired};
DetailsHUDBar.defaultProps = {};


export default DetailsHUDBar;
