//noinspection JSUnresolvedVariable
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { PanelGroup, Panel } from "react-bootstrap"
import Schedule from "../../Schedule/Schedule"
import ScheduleDay from "../../Schedule/ScheduleDay"
import ColorHash from "color-hash"
import {convertToShortWeekday, convertToLongWeekday, timeToString} from "../../../helper/timeHelpers"


class TimeSlotsDisplay extends Component {
    /** @namespace timeSlot.nonWeekly */
    /** @namespace timeSlot.startTime.minute */
    /** @namespace timeSlot.startTime.hour */
    /** @namespace this.props.section.timeSlots */
    /** @namespace timeSlot.daysList */

    constructor(props){
        super(props);
        this.refs = [];
        this.handleSelect = this.handleSelect.bind(this);
    }

    renderTimeSlotHeader(timeSlot){
        let days = (timeSlot.daysList.length > 1) ? timeSlot.daysList.map(d => convertToShortWeekday(d)).join("") : timeSlot.daysList.map(d => convertToLongWeekday(d)).join("");
        return <div>{days + " " + timeToString(timeSlot.startTime) + " - " + timeToString(timeSlot.endTime)}
            <div style={{float: "right"}}>{timeSlot.nonWeekly && "Non Weekly"}</div>
        </div>;
    }

    getTimeSlotDetails(timeSlot){
        let colorHash = new ColorHash({lightness: 0.65, saturation: 0.8});

        return this.props.section.timeSlots.filter(ts => ts.id === timeSlot.id).map(ts => {
            let rgb = colorHash.rgb(this.props.course.subject);
            return {
                title: <span><b>{this.props.course.subject}</b> {this.props.course.courseNumber} - {this.props.section.sectionName}</span>,
                rgb: rgb.join(),
                ...ts
            };
        });
    }

    handleSelect(selectedKey){
        this["schedule"+selectedKey].setScrollHeight();
    }

    render() {
        return (
            <PanelGroup defaultActiveKey={0} accordion>
                {this.props.section.timeSlots.map((timeSlot,i) =>
                    <Panel header={this.renderTimeSlotHeader(timeSlot)} eventKey={i} key={timeSlot.id} onEntering={() => this.handleSelect(i)} >
                        <div style={{height: 200}}>
                            <Schedule showHeaders={false} events={this.getTimeSlotDetails(timeSlot)} ref={schedule => this["schedule"+i] = schedule}>
                                <ScheduleDay displayDays={timeSlot.daysList} />
                            </Schedule>
                        </div>
                    </Panel>
                )}
            </PanelGroup>
        );
    }
}

/** @namespace PropTypes.object */
TimeSlotsDisplay.propTypes = {course: PropTypes.object.isRequired, section: PropTypes.object.isRequired};
TimeSlotsDisplay.defaultProps = {};

export default TimeSlotsDisplay;