import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { FaUser } from "react-icons/lib/fa"
import { Panel, Button } from "react-bootstrap"
import Radiator from "../../Radiator/Radiator"

class InstructorPanel extends Component {
    render() {
        return (
            <Panel className="big-space-above">
                <div className="clear">
                    <FaUser className="float-left" style={{fontSize: "3em", marginRight: 10}}/>
                    <div>
                        <h4 style={{margin: 0}}>{this.props.instructor.firstName} {this.props.instructor.lastName}</h4>
                        <small>{this.props.instructor.email}</small>
                    </div>
                </div>

                <div className="clear space-above">
                    <div className="center-text">
                        <b className="faded-text">RateMyProfessors Score</b><br />
                        <span className="space-above" style={{display: "inline-block", width: 80, height: 80}}>
                                        <Radiator invert={true} value={this.props.instructor.rmpRating} maxValue={5} initialAnimation={false}/>
                                    </span>
                    </div>
                </div>

                <blockquote className="space-above">{this.props.instructor.rmpLatestComment}</blockquote>
                {this.props.instructor.rmpId &&
                <div className="center-text space-above">
                    <Button target="_blank"
                            href={"https://www.ratemyprofessors.com/ShowRatings.jsp?tid=" + this.props.instructor.rmpId}
                            bsSize="sm" bsStyle="primary">View RMP Profile</Button>
                </div>
                }
            </Panel>
        );
    }
}

InstructorPanel.propTypes = {instructor: PropTypes.object.isRequired};
InstructorPanel.defaultProps = {};

export default InstructorPanel;