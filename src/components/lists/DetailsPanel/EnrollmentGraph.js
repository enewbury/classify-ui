import React, {Component} from "react"
import {connect} from "react-redux";
import {Line} from "react-chartjs-2";
import PropTypes from "prop-types";
import {fetchSectionChanges} from "../../../actions/courseDataActions";

class EnrollmentGraph extends Component {

    componentDidMount(){
        if(this.props.sectionChanges === undefined){
            this.props.actions.fetchSectionChanges(this.props.section.id);
        }
    }

    formatGraphData(sectionChanges){
        let endDate = new Date();
        let startDate = new Date(endDate);
        startDate.setDate(startDate.getDate() - 7);

        if(this.props.sectionChanges) {
            let data = sectionChanges.map(change => {
                return {
                    x: new Date(change.timeStamp.year, parseInt(change.timeStamp.monthValue, 10) - 1, change.timeStamp.dayOfMonth, change.timeStamp.hour, change.timeStamp.minute),
                    y: change.seatsFilled
                }
            });

            if( !data || data.length === 0){
                data.push({x: startDate, y: this.props.section.seatsFilled});
                data.push({x: endDate, y: this.props.section.seatsFilled});
            } else {
                data.unshift({x: startDate, y: data[0].y});
                data.push({x:endDate, y: data[data.length -1].y});
            }

            return data;
        } else {
            return [];
        }

    }

    render(){
        return <Line data={{
            datasets:[{
                data: this.formatGraphData(this.props.sectionChanges),
                backgroundColor: "rgba(54, 162, 235, 0.2)",
                borderColor: 'rgba(54, 162, 235, 0.3)',
                fill: true
            }]
        }} options={{
            legend: {
                display: false
            },
            title: {
                display: true,
                text: "Enrollment Over Time"
            },
            scales: {
                xAxes: [{
                    type: "time",
                    time: {
                        unit: "day"
                    }
                }],
                yAxes: [{
                    type: "linear",
                    ticks:{
                        labelString: "Percent Full",
                        suggestedMin:0,
                        suggestedMax: (this.props.section ) ? this.props.section.seats : 50,
                    }
                }]
            }
        }} height={100} />
    }
}

EnrollmentGraph.propTypes = {
    section: PropTypes.object.required,
};
EnrollmentGraph.defaultProps = {};


const mapStateToProps = (state, currentProps) => {
    return {
        sectionChanges: state.courseData.sectionChanges[currentProps.section.id]
    }
};

const mapDispatchToProps = (dispatch) => {
    return {actions: {
        fetchSectionChanges: (sectionId) => dispatch(fetchSectionChanges(sectionId))
    }};
};

export default connect(mapStateToProps, mapDispatchToProps)(EnrollmentGraph);