import React, {Component} from 'react';
import Schedule from "../Schedule/Schedule";
import ColorHash from "color-hash";
import {connect} from "react-redux";
import {getRootForEntry, isContainerEntry} from "../../helper/entryHelpers";
import {withRouter} from "react-router-dom";

class ListsScheduleViewer extends Component {

    constructor(props){
        super(props);
        this.colorHash = new ColorHash({lightness: 0.65, saturation: 0.8});
        this.getEvents = this.getEvents.bind(this);
        this.getChildren = this.getChildren.bind(this);
        this.getTimeSlotDetails = this.getTimeSlotDetails.bind(this);
        this.getTimeSlotsDetails = this.getTimeSlotsDetails.bind(this);
    }

    getEvents(){
        if(this.props.courseDataLoaded && this.props.entries[this.props.selectedEntryId] !== undefined){
            let selectedSchedule = getRootForEntry(this.props.entries[this.props.selectedEntryId], this.props.entries);
            let sectionEntries = this.getChildren(selectedSchedule);
            return this.getTimeSlotsDetails(sectionEntries.map(entry => this.props.sections[entry.sectionId]));
        } else {
            return [];
        }
    }

    getChildren(entry){
        let children = [];
        entry.entries.forEach(childId => {
            let child = this.props.entries[childId];
            if(isContainerEntry(child.type)){
                children = children.concat(this.getChildren(child));
            } else {
                children.push(child);
            }
        });

        return children;
    }

    getTimeSlotsDetails(sections){
        let timeSlotDetails = [];
        sections.forEach(section => {
            timeSlotDetails = timeSlotDetails.concat(this.getTimeSlotDetails(section));
        });
        return timeSlotDetails;
    }

    getTimeSlotDetails(section){

        let course = this.props.courses[section.courseId];
        return section.timeSlots.map(ts => {
            let rgb = this.colorHash.rgb(course.subject);
            return {
                title: <span><b>{course.subject}</b> {course.courseNumber} - {section.sectionName}</span>,
                rgb: rgb.join(),
                ...ts
            };
        });
    }

    render() {
        return (
            <div style={{height: "calc(100% - 65px)"}}>
                <Schedule events={this.getEvents()} />
            </div>
        );
    }
}

ListsScheduleViewer.propTypes = {};
ListsScheduleViewer.defaultProps = {};

const mapStateToProps = (state) => {
    return {
        courseDataLoaded: state.courseData.isLoaded[state.selectedTermId] === true,
        courses: state.courseData.entities.courses,
        sections: state.courseData.entities.sections,
        selectedEntryId: state.lists.selectedEntries[state.selectedTermId],
        entries: state.lists.entries
    }
};

export default withRouter(connect(mapStateToProps)(ListsScheduleViewer));