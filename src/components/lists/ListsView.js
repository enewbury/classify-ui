import React, {Component} from 'react';
import {Button, ButtonGroup, Col} from "react-bootstrap";
import {FaCalendar, FaPieChart} from "react-icons/lib/fa"
import ListsPanel from "./ListsPanel/ListsPanel";
import OpenTabs from "./OpenTabs/OpenTabs";
import ListsScheduleViewer from "./ListsScheduleViewer";
import {Redirect, Route, Switch} from "react-router-dom";
import {LinkContainer} from "react-router-bootstrap";
import AddMoveModal from "../AddMoveModal/AddMoveModal";
import FeedbackPanel from "../FeedbackPanel";

class ListsView extends Component {

    render() {
        return (
            <div className="full-height">
                <Col sm={4} className="border-right shade-2 full-height-auto">
                    <ListsPanel/>
                </Col>
                <Col sm={8} className="no-padding full-height-auto">
                    <div style={{height: 65, textAlign: "right"}}>
                        <span style={{padding: 15, display: "inline-block", borderLeft: "solid 1px #ddd"}}>
                            <ButtonGroup>
                                <LinkContainer to={`${this.props.match.url}/details`}><Button bsStyle="primary" bsSize="sm"><FaPieChart/> Course Details</Button></LinkContainer>
                                <LinkContainer to={`${this.props.match.url}/schedule`}><Button bsStyle="primary" bsSize="sm"><FaCalendar/> Schedule</Button></LinkContainer>
                            </ButtonGroup>
                        </span>
                    </div>
                    <Switch>
                        <Route exact path={`${this.props.match.url}/schedule`}>
                            <ListsScheduleViewer />
                        </Route>

                        <Route path={`${this.props.match.url}/details`}>
                            <OpenTabs />
                        </Route>

                        <Route path={`${this.props.match.url}`}><Redirect to={`${this.props.match.url}/details`} /></Route>
                    </Switch>
                </Col>
                <AddMoveModal />
                <div style={{position: "fixed", bottom: 0, right: 15, zIndex: 4}}><FeedbackPanel/></div>
            </div>
        );
    }
}

ListsView.propTypes = {};
ListsView.defaultProps = {};

export default ListsView;