import React, { Component } from "react"
import ColorHash from "color-hash"
import { FaTrash, FaArrows } from "react-icons/lib/fa"
import {connect} from "react-redux";
import * as scheduleActions from "../../../actions/listsActions";
import {OverlayTrigger, Popover, Button} from "react-bootstrap"
import PropTypes from "prop-types"
import {showMoveModal} from "../../../actions/addMoveModalActions";

class ScheduleSectionEntry extends Component {
    /** @namespace this.props.course */
    /** @namespace this.props.section.sectionName */
    /** @namespace this.props.section.subject */
    /** @namespace this.props.section.courseNumber */

    constructor(props){
        super(props);
        this.selectSection = this.selectSection.bind(this);
    }

    selectSection(){
        this.props.actions.selectSection(this.props.section.id, this.props.selectedTermId);
        this.props.actions.selectEntity(this.props.entry.id);
    }

    render(){
        if ( this.props.visible) {
            let selectedClass = this.props.selectedEntryId === this.props.entry.id ? " selected" : "";
            let classString = "schedule-tab schedule-section-tab" + selectedClass;
            let colorHash = new ColorHash({lightness: 0.65, saturation: 0.8});
            return <div className={classString} style={{paddingLeft: 35 * this.props.depth}} onClick={this.selectSection}>
                <div className="tools">
                    <button onClick={() => this.props.actions.showMoveModal(this.props.entry.id)}><FaArrows /></button>
                    <OverlayTrigger placement="right" trigger="click" rootClose overlay={
                        <Popover id={"delete section" + this.props.entry.id}>
                            Are you sure? &nbsp;<Button bsSize="sm" onClick={() => this.props.actions.removeSection(this.props.entry.id, this.props.entry.parentId)} bsStyle="danger">Delete</Button>
                        </Popover>
                    }>
                        <button onClick={(e) => e.stopPropagation()}><FaTrash /></button>
                    </OverlayTrigger>
                </div>
                <div className="list-name">
                    <b>
                        <span style={{color: colorHash.hex(this.props.course.subject)}}>
                            {this.props.course.subject}
                        </span> {this.props.course.courseNumber}
                    </b>&nbsp;
                    <span>{this.props.course.name} - {this.props.section.sectionName}</span>
                </div>
            </div>
        } else {
            return null;
        }
    }
}

ScheduleSectionEntry.propTypes = {entry: PropTypes.object.isRequired, visible: PropTypes.bool, section: PropTypes.object.isRequired, course: PropTypes.object.isRequired};
ScheduleSectionEntry.defaultProps = {depth: 1};

const mapStateToProps = (state) => {
    return {
        selectedEntryId: state.lists.selectedEntries[state.selectedTermId],
        selectedTermId: state.selectedTermId,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            showMoveModal: (entryId) => dispatch(showMoveModal(entryId, [])),
            selectSection: (sectionId, termId) => dispatch(scheduleActions.selectSection(sectionId, termId)),
            selectEntity: (entityId) => dispatch(scheduleActions.selectEntry(entityId)),
            removeSection: (entryId, parentId) => dispatch(scheduleActions.removeSection(entryId, parentId))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ScheduleSectionEntry);