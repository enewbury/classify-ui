import React, { Component } from "react"
import { FaFolderO, FaFolderOpenO, FaPencil, FaTrash, FaArrows } from "react-icons/lib/fa/"
import { OverlayTrigger, Button, Popover } from "react-bootstrap"
import EditInPlace from "./EditInPlace"
import { withDropdown } from "./withDropdown";
import * as scheduleActions from "../../../actions/listsActions"
import {connect} from "react-redux";
import {showMoveModal} from "../../../actions/addMoveModalActions";


class ScheduleGroupEntry extends Component {
    /** @namespace this.props.visible */
    /** @namespace this.props.group.name */
    /** @namespace child.isGroup */
    /** @namespace this.props.depth */
    constructor(props){
        super(props);
        this.handleSelect = this.handleSelect.bind(this);
        this.startEditing = this.startEditing.bind(this);
        this.updateName = this.updateName.bind(this);
        this.state = {editMode: false}
    }

    startEditing(e){
        e.stopPropagation();
        this.setState({editMode: true});
    }

    updateName(updatedName){
        this.setState({editMode: false});
        if(updatedName !== null){
            this.props.actions.updateName(this.props.group.id, updatedName);
        }
    }

    handleSelect(e){
        this.props.actions.selectEntry(this.props.entryId);
        this.props.toggleDropdown(e);
    }

    render(){
        const openClass = this.props.open ? "open" : '';
        const selectedClass = this.props.selectedEntryId === this.props.entryId ? " selected" : "";
        const classString = "clear schedule-tab schedule-group-tab " + openClass + selectedClass;

        return <div style={{display: (this.props.visible) ? "block": "none" }} className="schedule-group">
            <div className={classString} style={{paddingLeft:35*this.props.depth}} onClick={this.handleSelect}>
                <div className="tools">
                    <FaPencil onClick={this.startEditing} />&nbsp;&nbsp;
                    <button onClick={(e) => {e.stopPropagation(); this.props.actions.showMoveModal(this.props.entryId)}}><FaArrows /></button>
                    <OverlayTrigger placement="right" trigger="click" rootClose overlay={
                        <Popover id={"delete " + this.props.group.id}>
                            Are you sure? &nbsp;<Button bsSize="sm" onClick={() => this.props.actions.deleteGroup(this.props.group.id, this.props.group.parentId)} bsStyle="danger">Delete</Button>
                        </Popover>
                    }>
                        <button onClick={(e) => e.stopPropagation()}><FaTrash /></button>
                    </OverlayTrigger>
                </div>
                <div className="list-name">
                    {(this.props.open) ? <FaFolderOpenO className="panel-icon float-left"/> : <FaFolderO className="panel-icon float-left"/> }
                    <div className="float-left">
                        <b className="relative">{this.state.editMode && <EditInPlace stopEditing={this.updateName} value={this.props.group.name}/>}{this.props.group.name}</b>
                        <br/>
                        <small>{this.props.group.entries.length} item{this.props.group.entries.length === 1 ? '' : 's'}</small>
                    </div>
                </div>
            </div>
            {this.props.open &&
                <ul className="group-entries">
                    {this.props.children}
                </ul>
            }
        </div>
    }
}

const mapStateToProps = (state) => {
    return {
        selectedEntryId: state.lists.selectedEntries[state.selectedTermId]
    };
};

const mapDispatchToProps = (dispatch) => {

    return {
        actions: {
            showMoveModal: (entryId) => dispatch(showMoveModal(entryId, [entryId])),
            selectEntry: (entryId) => dispatch(scheduleActions.selectEntry(entryId)),
            updateName: (entryId, name) => dispatch(scheduleActions.updateGroupName(entryId, name)),
            deleteGroup: (entryId, parentId) => dispatch(scheduleActions.deleteGroup(entryId, parentId))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(withDropdown(ScheduleGroupEntry));