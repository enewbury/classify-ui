import React, {Component} from 'react';
import ScheduleGroupEntry from "./ScheduleGroupEntry"
import ScheduleSectionEntry from "./ScheduleSectionEntry"
import PropTypes from 'prop-types';
import {ListEntryType} from "../../../helper/constants";
import {connect} from "react-redux";
import {sortEntriesAlphabetically} from "../../../helper/entryHelpers";

export const withDropdown = (WrappedComponent) => {
    class ComponentWithDropdown extends Component {

        constructor(props) {
            super(props);
            this.toggleDropdown = this.toggleDropdown.bind(this);
            this.renderChildren = this.renderChildren.bind(this);
            this.groupMatchesFilter = this.groupMatchesFilter.bind(this);
            this.sectionMatchesFilter = this.sectionMatchesFilter.bind(this);
            this.state = {
                open: localStorage.getItem("entry" + this.props.group.id) === 'true' || false,
            };
        }

        toggleDropdown(e) {
            localStorage.setItem("entry" + this.props.group.id, !this.state.open);
            this.setState({open: !this.state.open});
        }

        groupMatchesFilter(filter, group){
            return (filter === '' || group.name.toLowerCase().includes(filter));
        }

        sectionMatchesFilter(filter, course){
            return (filter === '' ||
            course.name.toLowerCase().includes(filter) ||
            (course.subject + " " + course.courseNumber).toLowerCase().includes(filter));
        }

        renderChildren(filter, depth, group, parentMatches) {
            let sortedIds = sortEntriesAlphabetically(group.entries || [], this.props.listEntries, this.props.courses, this.props.sections);

            return sortedIds.map((childId) => {
                let child = this.props.listEntries[childId];
                switch(child.type){
                    case ListEntryType.SCHEDULE:
                    case ListEntryType.GROUP:
                        let matchesFilter = this.groupMatchesFilter(filter, child);
                        let children = this.renderChildren(filter, depth + 1, child, parentMatches || matchesFilter);
                        // Filters children down to ones that are visible and checks if one or more exist
                        let childVisible = (children.filter((child) => child.props.visible).length > 0);
                        return <ScheduleGroupEntry entryId={childId} visible={parentMatches || matchesFilter || childVisible} filterValue={this.props.filterValue} key={child.id} group={child} depth={depth + 1}>
                            {children}
                        </ScheduleGroupEntry>;
                    case ListEntryType.SECTION:
                        let section = this.props.sections[child.sectionId];
                        let course = this.props.courses[section.courseId];
                        return <ScheduleSectionEntry entry={child} visible={parentMatches || this.sectionMatchesFilter(filter, course)} key={child.id} section={section} course={course} depth={depth + 1}/>;

                    default:
                        return null;
                }
            }, this);
        }

        render() {
            let filter = this.props.filterValue.toLowerCase();
            let matchesFilter = this.groupMatchesFilter(filter, this.props.group);
            let children = this.props.children || this.renderChildren(filter, this.props.depth, this.props.group, matchesFilter);
            let childVisible = (children.filter((child) => child.props.visible).length > 0);
            return <WrappedComponent visible={this.props.visible || childVisible || matchesFilter} {...this.props} {...this.state} toggleDropdown={this.toggleDropdown}>
                {children}
            </WrappedComponent>
        }

    }

    ComponentWithDropdown.propTypes = {group: PropTypes.object.isRequired, depth: PropTypes.number, filterValue: PropTypes.string.isRequired};
    ComponentWithDropdown.defaultProps = {depth: 1};

    const mapStateToProps = (state) => {
        return {
            sections: state.courseData.entities.sections,
            courses: state.courseData.entities.courses,
            listEntries: state.lists.entries,
        }
    };

    return connect(mapStateToProps)(ComponentWithDropdown);
};
