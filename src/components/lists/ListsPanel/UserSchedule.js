import React, {Component} from "react";
import { FaListUl, FaPencil, FaTrash } from "react-icons/lib/fa/"
import {Button, OverlayTrigger, Popover} from "react-bootstrap"
//necessary to avoid circular dependency issue
/* eslint-disable no-unused-vars */
//noinspection ES6UnusedImports
import ScheduleGroupEntry from "./ScheduleGroupEntry";
/* eslint-enable no-unused-vars */
import EditInPlace from "./EditInPlace"
import { withDropdown } from "./withDropdown";
import {connect} from "react-redux";
import * as listsActions from "../../../actions/listsActions"
import PropTypes from "prop-types"

class UserSchedule extends Component {
    /** @namespace this.props.group */
    constructor(props){
        super(props);
        this.state = {editMode: false};
        this.handleSelect = this.handleSelect.bind(this);
        this.startEditing = this.startEditing.bind(this);
        this.updateName = this.updateName.bind(this);
    }

    startEditing(e){
        e.stopPropagation();
        this.setState({editMode: true});
    }

    updateName(updatedName){
        this.setState({editMode: false});
        if(updatedName !== null){
            this.props.actions.updateName(this.props.group.id, updatedName);
        }
    }


    handleSelect(e){
        this.props.actions.selectEntry(this.props.group.id);
        this.props.toggleDropdown(e);
    }



    render(){
        const openClass = this.props.open ? " open" : '';
        const selectedClass = this.props.selectedEntryId === this.props.group.id ? " selected" : "";
        const classString = "schedule-tab schedule-entry-tab clear" + openClass + selectedClass;
        return <div style={{display: (this.props.visible) ? "block": "none" }} className="UserSchedule default-border-color">
            <div className={classString} onClick={this.handleSelect}>
                <div className="tools">
                    <FaPencil onClick={this.startEditing}/>&nbsp;&nbsp;
                    <OverlayTrigger placement="right" trigger="click" rootClose overlay={
                        <Popover id={"delete-schedule-"+this.props.group.id}>
                            Are you sure? &nbsp;<Button bsSize="sm" onClick={() => this.props.actions.deleteSchedule(this.props.group.id)} bsStyle="danger">Delete</Button>
                        </Popover>
                    }>
                        <button onClick={(e)=>e.stopPropagation()}><FaTrash /></button>
                    </OverlayTrigger>
                    {/*{(this.props.open) ? <FaAngleDown /> : <FaAngleLeft />}*/}
                </div>
                <div className="list-name">

                    <FaListUl className="panel-icon float-left"/>
                    <div className="text-section float-left">
                        <b className="relative">
                            {this.state.editMode && <EditInPlace stopEditing={this.updateName} value={this.props.group.name}/>}
                            {this.props.group.name}
                        </b><br/>
                        <small>{this.props.group.entries.length} item{this.props.group.entries.length === 1 ? '' : 's'}</small>
                    </div>
                </div>

            </div>
            {this.props.open &&
                <ul className="schedule-children">
                    {this.props.children}
                </ul>
            }
        </div>
    }
}

UserSchedule.propTypes = {group: PropTypes.object.isRequired, open: PropTypes.bool, visible: PropTypes.bool};
UserSchedule.defaultProps = {};

const mapStateToProps = (state) => {
    return {
        selectedEntryId: state.lists.selectedEntries[state.selectedTermId]
    };
};

const mapDispatchToProps = (dispatch, props) =>{

    return {
        actions: {
            selectEntry: (entryId) => dispatch(listsActions.selectEntry(entryId)),
            updateName: (entryId, scheduleName) => dispatch(listsActions.updateScheduleName(entryId, scheduleName)),
            deleteSchedule: (entryId) => dispatch(listsActions.deleteSchedule(entryId))
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(withDropdown(UserSchedule));