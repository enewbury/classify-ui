import React from 'react';
import ReactDOM from "react-dom"
import {FormControl, Popover, OverlayTrigger, Overlay} from "react-bootstrap";
import UserSchedule from "./UserSchedule"
import { TiFolderAdd } from "react-icons/lib/ti"
import { MdPlaylistAdd } from "react-icons/lib/md"
import {addNewSchedule, addNewFolder} from "../../../actions/listsActions"
import "./ListsPanel.css"
import {connect} from "react-redux";
import {isContainerEntry, sortEntriesAlphabetically, filterEntriesByAttributes} from "../../../helper/entryHelpers";
import {ListEntryType} from "../../../helper/constants"

class ListsPanel extends React.Component {
    constructor(props) {
        super(props);
        this.filterSchedules = this.filterSchedules.bind(this);
        this.createNewSchedule = this.createNewSchedule.bind(this);
        this.createNewFolder = this.createNewFolder.bind(this);
        this.state = {filterValue: ''};
    }

    filterSchedules(e){
        this.setState({ filterValue: e.target.value });
    };

    createNewSchedule(e){
        e.preventDefault();
        this.newScheduleTrigger.hide();
        this.props.actions.addNewSchedule(ReactDOM.findDOMNode(this.newScheduleInput).value, this.props.selectedTermId);
    }

    createNewFolder(e){
        e.preventDefault();
        this.newFolderTrigger.hide();
        let parentId = (isContainerEntry(this.props.selectedListEntry.type)) ? this.props.selectedListEntry.id : this.props.selectedListEntry.parentId;
        this.props.actions.addNewFolder(this.props.selectedTermId, parentId, ReactDOM.findDOMNode(this.newFolderInput).value);
    }

    render() {

        let sortedScheduleIds = sortEntriesAlphabetically(Object.keys(this.props.schedules), this.props.schedules);

        const userScheduleList = (this.props.courseDataLoaded ) ? sortedScheduleIds.map((id) => {
            return <li key={id}>
                    <UserSchedule className="default-border-color" filterValue={this.state.filterValue} group={this.props.schedules[id]}/>
            </li>
        }) : null;

        return (
            <div className="ListsPanel">
                <div className="panel-controls">
                    <FormControl
                        id="list-filter-box"
                        type="text"
                        value={this.state.filterValue}
                        placeholder="Filter Lists"
                        onChange={this.filterSchedules}
                    />
                    <div style={{borderTop: "solid 1px #ddd"}}>
                        <div className="new-folder-schedule-buttons" style={{paddingRight: 15, fontSize: "2em", float: "right"}}>
                            <OverlayTrigger ref={(trg) => this.newScheduleTrigger = trg} placement="bottom" trigger="click" rootClose overlay={
                                <Popover id="new-schedule-popover" title="New Schedule">
                                    <form onSubmit={this.createNewSchedule}>
                                    <FormControl
                                    id="new-schedule-input"
                                    type="text"
                                    ref={(input) => this.newScheduleInput = input}
                                    placeholder="Schedule Name" />
                                    </form>
                                </Popover>
                            }>
                                <button disabled={!this.props.selectedTermId}><MdPlaylistAdd /></button>
                            </OverlayTrigger>
                            <Overlay show={this.props.courseDataLoaded && Object.keys(this.props.schedules).length === 0} target={this.newScheduleTrigger} placement="top">
                                <Popover id="new-schedule-prompt-popover">Create a new Schedule to Get Started</Popover>
                            </Overlay>
                            &nbsp;
                            <OverlayTrigger ref={(trg) => this.newFolderTrigger = trg} placement="bottom" trigger="click" rootClose overlay={
                                <Popover id="new-folder-popover" title="New Folder">
                                    <form onSubmit={this.createNewFolder}>
                                        <FormControl
                                            id="new-folder-input"
                                            type="text"
                                            ref={(input) => this.newFolderInput = input}
                                            placeholder="Folder Name" />
                                    </form>
                                </Popover>
                            }>
                                <button disabled={!this.props.selectedTermId}><TiFolderAdd/></button>
                            </OverlayTrigger>
                        </div>
                        <div className="me-friends-filter"><h4 style={{padding: "8px 15px"}}>My Schedules</h4>{/*<a className="selected">All</a><a>Me</a><a>Friends</a>*/}</div>
                    </div>
                </div>

                <ul>{userScheduleList}</ul>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        courseDataLoaded: state.courseData.isLoaded[state.selectedTermId] === true,
        schedules: filterEntriesByAttributes(state.lists.entries, {termId: state.selectedTermId, parentId: null, type: ListEntryType.SCHEDULE}),
        selectedTermId: state.selectedTermId,
        selectedListEntry: state.lists.entries[state.lists.selectedEntries[state.selectedTermId]] || null,
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            addNewSchedule: (scheduleName, termId) => dispatch(addNewSchedule(scheduleName, termId)),
            addNewFolder: (termId, parentId, folderName) => dispatch(addNewFolder(termId, parentId, folderName))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ListsPanel);
