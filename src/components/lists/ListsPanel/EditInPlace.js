import React, {Component} from 'react';
import {FormControl } from "react-bootstrap"
import ReactDOM from "react-dom"
import PropTypes from 'prop-types';

class EditInPlace extends Component {
    constructor(props){
        super(props);
        this.stopEditing = this.stopEditing.bind(this);
        this.nameEdited = this.nameEdited.bind(this);
    }

    componentDidMount(){
        let input = ReactDOM.findDOMNode(this.formControlRef);
        input.focus();
        input.select();
    }

    nameEdited(e) {
        e.stopPropagation();
        if(e.charCode===13){
            this.props.stopEditing(e.target.value);
        }
    }

    stopEditing(e){
        e.stopPropagation();
        this.props.stopEditing(null);
    }

    render() {
        return (
            <FormControl
                className="in-place-input"
                type="text"
                ref={(c)=>this.formControlRef=c}
                onBlur={this.stopEditing}
                onClick={(e) => e.stopPropagation()}
                onKeyPress={this.nameEdited}
                defaultValue={this.props.value}
                placeholder="Schedule Name"/>
        );
    }
}

EditInPlace.propTypes = {value: PropTypes.string, stopEditing: PropTypes.func};
EditInPlace.defaultProps = {};

export default EditInPlace;