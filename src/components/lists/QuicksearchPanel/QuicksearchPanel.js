import React, {Component} from 'react';
import "./Quicksearch.css"
import { FormControl } from "react-bootstrap"
import SearchResult from "../../layout/SearchResult"
import Instructions from "./Instructions"
import ReactList from "react-list"
import { FaBinoculars, FaInfoCircle } from "react-icons/lib/fa"
import {connect} from "react-redux";

class QuicksearchPanel extends Component {
    constructor(){
        super();
        this.showInstructions = this.showInstructions.bind(this);
        this.hideInstructions = this.hideInstructions.bind(this);
        this.renderResult = this.renderResult.bind(this);
        this.filterList = this.filterList.bind(this);
        this.state = {instructionsOpen: false, query: null, matchers: null, results: []};
    }

    showInstructions(){
        this.setState({instructionsOpen: true})
    }

    hideInstructions() {
        this.setState({instructionsOpen: false})
    }

    getFilters(query){
        let matchers = /by |on |at |before |after /;
        let nameMatcher = query.split(matchers)[0].trim();
        let instructorMatcher = (query.split("by ")[1] || "").split(matchers)[0].trim();
        return {nameMatcher, instructorMatcher};
    }

    matchesNameFilter(c, nameMatcher){
        return (!nameMatcher ||
        c.name.toLowerCase().includes(nameMatcher) ||
        (c.subject + " " + c.courseNumber).toLowerCase().includes(nameMatcher)
        );
    }

    matchesInstructorFilter(i, instructorMatcher){
        return !instructorMatcher ||
            (i && (i.firstName + " " + i.lastName).toLowerCase().includes(instructorMatcher));
    }

    filterList(e){
        let query = e.target.value.toLowerCase();
        if (query.length > 1) {
            let matchers = this.getFilters(query);
            let {nameMatcher, instructorMatcher} = matchers;
            let results = Object.keys(this.props.sections).reduce((results, sectionId) => {
                let s = this.props.sections[sectionId];
                let c = this.props.courses[s.courseId];
                let i = this.props.instructors[s.instructorId];
                if (s.termId === this.props.termId &&
                    this.matchesNameFilter(c, nameMatcher) &&
                    this.matchesInstructorFilter(i, instructorMatcher)){
                    results.push({course: c, section: s, instructor: i});
                }
                return results
            }, []);

            this.setState({query: query, results: results, matchers: matchers});
        } else {
            this.setState({query: query, results: []});;
        }
    }

    renderResult(index, key){
        let course = this.state.results[index].course;
        let section = this.state.results[index].section;
        let instructor = this.state.results[index].instructor;

        return <SearchResult key={key} matchers={this.state.matchers} course={course} section={section} instructor={instructor} />
    }


    render() {

        return (
            <div className="QuicksearchPanel">
                <div style={{padding: 20}} className="clear">

                    <FormControl
                        style={{display: "inline-block", width: "calc(100% - 40px)"}}
                        type="text"
                        placeholder="Quicksearch"
                        onChange={this.filterList}
                    />
                    <FaInfoCircle onClick={this.showInstructions} style={{marginLeft: 15, cursor: "pointer"}} />
                    <Instructions show={this.state.instructionsOpen} close={this.hideInstructions}/>

                </div>
                <table>
                    {/*{this.state.results.map(r => this.renderResult(r.course, r.section, r.instructor))}*/}
                </table>
                <div className="base-bg-color">
                    {/* Results */}
                    {this.state.results.length > 0 &&
                    <div style={{overflow: 'auto', maxHeight: 400}}>
                        <ReactList
                            itemRenderer={this.renderResult}
                            length={this.state.results.length}
                            type='uniform'
                        />
                    </div>
                    }

                    {/* Default Empty Search Page */}
                    {!this.state.query &&
                        <div style={{padding: 50}}>
                            <div style={{margin: "30px auto", maxWidth: 500, textAlign: "center"}}>
                                <FaBinoculars style={{height: 45, width: 45, fill: "#888"}}/>
                                <h3 style={{color: "#777"}}>Search Courses and Instructors</h3>
                                <p>Use the Quicksearch bar above to view courses and add them to your schedules, or click on an
                                    item in an existing schedule to expand it.</p>
                            </div>
                        </div>
                    }
                </div>
            </div>
        );
    }
}

QuicksearchPanel.propTypes = {};
QuicksearchPanel.defaultProps = {};

let mapStateToProps = (state) => {
    return {
        sections: state.courseData.entities.sections,
        courses: state.courseData.entities.courses,
        instructors: state.courseData.entities.instructors,
        termId: state.selectedTermId
    }
};

export default connect(mapStateToProps)(QuicksearchPanel);