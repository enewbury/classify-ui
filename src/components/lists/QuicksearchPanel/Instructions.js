import React, {Component} from 'react';
import { Modal, Button } from "react-bootstrap"
import PropTypes from 'prop-types';

class Instructions extends Component {
    render() {
        return (
            <Modal show={this.props.show} onHide={this.props.close}>
                <Modal.Header><h3>Quicksearch Instructions</h3></Modal.Header>
                <Modal.Body>
                    <p>Search for Courses by subject, course number, and course name. Add the BY keyword to filter by instructor</p>
                    <p>
                        <b>Examples</b><br />
                        <code>ASCI 001 <b>BY</b> John Doe</code><br />
                        <code>Kickball <b>BY</b> Curious George</code>
                    </p>
                    <dl>
                        <dt>BY</dt><dd>Filter results to instructors whose name matches value after the BY keyword</dd>
                        {/*<dt>ON</dt><dd>Filter results to sections with timeslots matching day of week key (Mo,Tu,We,Th,Fr,Sa,Su).  Multiple days separated by commas.</dd>*/}
                        {/*<dt>AT/BEFORE/AFTER</dt><dd>Show only results before, after, or starting at the time specified in (h:mmAM) format</dd>*/}
                    </dl>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.props.close}>Close</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

Instructions.propTypes = {close: PropTypes.func, show: PropTypes.bool};
Instructions.defaultProps = {};

export default Instructions;