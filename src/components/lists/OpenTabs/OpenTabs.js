import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { FaClose, FaPlus } from "react-icons/lib/fa/";
import { Tab, Panel, Nav, NavItem } from "react-bootstrap";
import queryString from "query-string"
import ColorHash from "color-hash"
import DetailsPanel from "../DetailsPanel/DetailsPanel";
import QuicksearchPanel from "../QuicksearchPanel/QuicksearchPanel"
import {connect} from "react-redux";
import * as tabActions from "../../../actions/tabActions"
import "./OpenTabs.css"
import {withRouter} from "react-router-dom";
import * as scheduleActions from "../../../actions/listsActions";

class OpenTabs extends Component {
    /** @namespace section.instructorId */
    /** @namespace section.courseId */

    constructor(props){
        super(props);
        this.selectTab = this.selectTab.bind(this);
        this.generateTabData = this.generateTabData.bind(this);
        this.closeTab = this.closeTab.bind(this);

        //select tab
        let sectionId = queryString.parse(this.props.location.search).section;
        if(sectionId){
            props.actions.selectSection(sectionId, this.props.termId);
        }
    }

    selectTab(index){
        this.props.actions.selectTab(index, this.props.termId);
    }

    closeTab(index, e){
        e.stopPropagation();
        this.props.actions.closeTab(index, this.props.termId);
    }
    
    generateTabData(selectedSection){
        try {
            let section = this.props.sections[selectedSection];
            let course = this.props.courses[section.courseId];
            let instructor = this.props.instructors[section.instructorId];
            return {section, course, instructor}
        } catch (error){
            console.log(error);
            return {error: true}
        }
    }

    render() {
        var tabs, tabContents;

        if(this.props.courseDataLoaded === true) {
            // Generate all the tabs
            tabs = this.props.openTabs.map((sectionId, i) => {
                let tabData = this.generateTabData(sectionId);
                if (tabData.error) {
                    return <NavItem eventKey={i} key={i}>Error</NavItem>;
                } else {
                    let colorHash = new ColorHash({lightness: 0.55, saturation: 0.8});
                    return <NavItem eventKey={i} key={i}>
                        <span style={{color: colorHash.hex(tabData.course.subject)}}>{tabData.course.subject}</span>&nbsp;
                        <span>{tabData.course.courseNumber}</span>&nbsp;-&nbsp;
                        <span>{tabData.section.sectionName}</span>
                        <FaClose onClick={(e) => this.closeTab(i,e)} className="close-tab"/>
                    </NavItem>
                }
            });

            //Generate all the tab panels
            tabContents = this.props.openTabs.map((sectionId, i) => {
                let tabData = this.generateTabData(sectionId);
                if (tabData.error) {
                    return <Tab.Pane eventKey={i} key={i}>
                        <div style={{margin: 15}}>
                            <Panel header="Error" bsStyle="danger">
                                <p>There was a problem loading this section. Try refreshing the page and trying
                                    again.</p>
                            </Panel>
                        </div>
                    </Tab.Pane>
                } else {
                    return <Tab.Pane eventKey={i} key={i}>
                        <DetailsPanel course={tabData.course} section={tabData.section} instructor={tabData.instructor}
                                      sectionId={sectionId}/>
                    </Tab.Pane>
                }
            });
        }

        //generate the structure and load in the panels
        return (
            <div className="OpenTabs">

                <Tab.Container activeKey={this.props.selectedTab} onSelect={this.selectTab} id="open-tabs">
                    <div style={{marginTop: -30}}>
                        <div style={{overflow: "hidden", width: "calc(100% - 250px)", marginBottom: -1}}>
                            <div style={{marginBottom: -15}}>
                                <div className="tab-row">
                                    <Nav bsStyle="tabs">
                                        {tabs}
                                        <NavItem eventKey={-1}><FaPlus /></NavItem>
                                    </Nav>
                                </div>
                            </div>
                        </div>
                        <Tab.Content animation={false}>
                            {tabContents}

                            <Tab.Pane eventKey={-1}>
                                <QuicksearchPanel />
                            </Tab.Pane>
                        </Tab.Content>
                    </div>
                </Tab.Container>
            </div>
        );
    }
}

OpenTabs.propTypes = {openTabs: PropTypes.array};
OpenTabs.defaultProps = {};

const mapStateToProps = (state) => {
    return {
        termId: state.selectedTermId,
        openTabs: (state.tabs[state.selectedTermId] || {openTabs: []}).openTabs,
        selectedTab: (state.tabs[state.selectedTermId] || {selectedTab: -1}).selectedTab,
        courseDataLoaded: state.courseData.isLoaded[state.selectedTermId] === true,
        sections: state.courseData.entities.sections,
        courses: state.courseData.entities.courses,
        instructors: state.courseData.entities.instructors
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            selectSection: (sectionId, termId) => dispatch(scheduleActions.selectSection(sectionId, termId)),
            closeTab: (index, termId) => dispatch(tabActions.closeTab(index, termId)),
            selectTab: (index, termId) => dispatch(tabActions.selectTab(index, termId))
        }
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(OpenTabs));