import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

class DonationReminder extends Component {
    constructor(props){
        super(props);
        this.state = {open: !this.props.donated};
    }

    render() {
        return (
            (!this.state.open) ? null : <div style={{width: "100%", position: "fixed", zIndex: 1, bottom: 0, left: 0, padding: 15, borderTop: "solid 1px #ddd", textAlign: "center", backgroundColor: "#eee"}}>
                <span style={{marginRight: 15}}>Make a donation now, and use Classify free forever!</span>
                <span><LinkContainer to="/donate"><Button bsSize="sm" onClick={()=> this.setState({open: false})} bsStyle="primary">Learn More</Button></LinkContainer>&nbsp;&nbsp;<Button onClick={()=> this.setState({open: false})} bsSize="sm">Close</Button></span>
            </div>
        );
    }
}

DonationReminder.propTypes = {};
DonationReminder.defaultProps = {};

const mapStateToProps = (state) => {
    return {
        donated: state.user.donated || localStorage.getItem("donated")
    }
};

export default connect(mapStateToProps)(DonationReminder);