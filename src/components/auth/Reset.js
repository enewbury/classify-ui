import React, {Component} from 'react';
import {API, JSON_HEADERS, SERVER} from "../../helper/constants";
import {Button, Form, FormControl, FormGroup, Overlay, Panel, Tooltip} from "react-bootstrap";
import queryString from "query-string"
import {Redirect} from "react-router-dom";

class Reset extends Component {

    constructor(props){
        super(props);
        this.state = {
            processing: false,
            passwordValue: "",
            confirmValue: "",
            passwordValid: true,
            confirmValid: true,
            validationMessage: "",
            focusedInput: null,
            result: null
        };
        this.updatePassword = this.updatePassword.bind(this);
        this.updateConfirm = this.updateConfirm.bind(this);
        this.submit = this.submit.bind(this);
    }

    updatePassword(e){
        this.setState({passwordValue: e.target.value});
        if(e.target.value.length < 6){
            this.setState({
                passwordValid: false,
                validationMessage: "Password must be at least 6 characters"
            });
        } else {
            this.setState({
                passwordValid: true
            });
        }
    }

    updateConfirm(e){
        this.setState({confirmValue: e.target.value});
        if(e.target.value !== this.state.passwordValue){
            this.setState({
                confirmValid: false,
                validationMessage: "Does not match password"
            });
        } else {
            this.setState({
                confirmValid:true
            });
        }
    }

    submit(e){
        e.preventDefault();
        let token = queryString.parse(this.props.location.search).token;
        let networkErrorOutput = <Panel bsStyle="danger">Looks like we can't connect to the server right now!</Panel>;

        if(!token){
            this.setState({output: <Panel bsStyle="warning">No token parameter to validate.</Panel>})
        } else {
            this.setState({processing: true});
            fetch(SERVER + API + "/user/token", {
                method: "PATCH",
                headers: JSON_HEADERS,
                body: JSON.stringify({token: token, type: "PASSWORD", password: this.state.passwordValue})
            }).then(
                response => {
                    if (response.ok) {
                        this.setState({processing: false, result: <Panel bsStyle="success">Reset! Redirecting to login.</Panel>});
                        setTimeout(() => this.setState({result: <Redirect to="/login"/>}), 1500);
                    } else {
                        response.json().then(json => {
                            this.setState({processing: false, result: <Panel bsStyle="danger">{json.message}</Panel>});
                        });
                    }
                },
                error => {
                    this.setState({processing: false, result: networkErrorOutput})
                }
            );
        }
    }

    render() {
        return (
            <Form onSubmit={this.submit} className="auth-form">
                <h3>Reset your password.</h3>
                {(this.state.result) && this.state.result }
                <Overlay show={(this.state.focusedInput === this.passwordInput) ? !this.state.passwordValid : !this.state.confirmValid} target={this.state.focusedInput}>
                    <Tooltip id="validation-tooltip">{this.state.validationMessage}</Tooltip>
                </Overlay>
                    <div>
                        <p>Enter your email address and we'll send you a link to reset your password.</p>
                        <FormGroup validationState={(!this.state.passwordValid) ? "warning" : null}>
                            <FormControl type="password" value={this.state.passwordValue} onChange={this.updatePassword} ref={(input) => this.passwordInput = input} onFocus={(e)=>this.setState({focusedInput: this.passwordInput})}/>
                        </FormGroup>
                        <FormGroup validationState={(!this.state.confirmValid) ? "warning" : null}>
                            <FormControl type="password" value={this.state.confirmValue} onChange={this.updateConfirm} ref={(input) => this.confirmInput = input} onFocus={(e)=>this.setState({focusedInput: this.confirmInput})}/>
                        </FormGroup>
                            <Button disabled={this.state.processing} type="submit" bsStyle="primary">Reset</Button>
                    </div>


            </Form>
        );
    }
}

Reset.propTypes = {};
Reset.defaultProps = {};

export default Reset;