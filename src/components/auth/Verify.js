import React, {Component} from 'react';
import {API, SERVER} from "../../helper/constants";
import {Panel} from "react-bootstrap";
import queryString from "query-string"
import {Redirect} from "react-router-dom";

class Verify extends Component {

    constructor(props){
        super(props);
        this.state = {
            output: <Panel bsStyle="info"><h3>Verifying your account...</h3></Panel>
        }
    }

    componentDidMount(){
        let token = queryString.parse(this.props.location.search).token;
        let networkErrorOutput = <Panel bsStyle="danger">Looks like we can't connect to the server right now!</Panel>;

        if(!token){
            this.setState({output: <Panel bsStyle="warning">No token parameter to validate.</Panel>})
        } else {
            fetch(SERVER + API + "/user/token", {
                method: "PATCH",
                headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
                body: JSON.stringify({token, type: "VERIFICATION"})
            }).then(
                response => {
                    if (response.ok) {
                        this.setState({output: <Panel bsStyle="success"><h3>Verified! Redirecting to login.</h3></Panel>});
                        setTimeout(() => this.setState({output: <Redirect to="/login"/>}), 1500);
                    } else {
                        response.json().then(json => {
                            this.setState({output: <Panel bsStyle="danger">{json.message}</Panel>});
                        });
                    }
                },
                error => {
                    this.setState({output: networkErrorOutput})
                }
            );
        }
    }

    render() {
        return (
            <div style={{margin: "50px auto", textAlign: "center", maxWidth: 400}}>{this.state.output}</div>
        );
    }
}

Verify.propTypes = {};
Verify.defaultProps = {};

export default Verify;