import {connect} from "react-redux";
import {Redirect, Route, withRouter} from "react-router-dom";
import * as React from "react";

const PrivateRoute = ({ component: Component, authenticated, ...rest }) => (
    <Route {...rest} render={(props) => {
            if (authenticated) {
                return <Component {...props}/>
            } else {
                return <Redirect to={{
                    pathname: '/login',
                    state: {from: props.location}
                }}/>
            }
        }
    }/>
);

const mapStateToProps = (state) => {
    return {
        authenticated: state.user.authenticated
    }
};
export default withRouter(connect(mapStateToProps)(PrivateRoute));