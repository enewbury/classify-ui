import React, {Component} from 'react';
import {FormControl, Form, Button, Panel, Checkbox} from "react-bootstrap"
import {attemptLogin, resendVerificationEmail} from "../../actions/userActions";
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import "./authForms.css"
import logo from "../layout/Header/logo.png";
import {Link, Redirect, withRouter} from "react-router-dom";
import {findDOMNode} from "react-dom";

class Login extends Component {

    constructor(props){
        super(props);
        this.state = {emailValue: "", passwordValue: "", rememberMe: false};
        this.attemptLogin = this.attemptLogin.bind(this);
        this.resendVerification = this.resendVerification.bind(this);
    }

    attemptLogin(e){
        e.preventDefault();
        let formData = "username=" + this.state.emailValue + "&password=" + this.state.passwordValue;
        if(this.state.rememberMe) formData += "&remember-me=on";
        this.props.actions.attemptLogin(formData);
    }

    resendVerification(){
        let email = findDOMNode(this.emailInput).value;
        this.props.actions.resendVerification(email);
        this.setState({notice: "Verification email sent to " + email});
    }

    render() {

        if(this.props.authenticated) {
            const {from} = this.props.location.state || {from: {pathname: '/lists'}};
            return <Redirect to={from}/>
        } else {

            const error = (!this.props.error) ? null : (this.props.error.exception === "DisabledException") ?
                <span>{this.props.error.message}. <a onClick={this.resendVerification}>Resend Verification Email?</a></span> :
                <span>{this.props.error.message}</span>;
            return (
                <Form className="auth-form" onSubmit={this.attemptLogin}>
                    <img style={{width: 40, marginTop: 20}} src={logo} alt="Classify Logo"/>
                    <h2>Welcome Back!</h2>
                    {this.props.error && !this.state.notice &&
                        <Panel bsStyle="danger">{error}</Panel>
                    }
                    {this.state.notice &&
                        <Panel bsStyle="info">{this.state.notice}</Panel>
                    }
                    <FormControl placeholder="Email" spellCheck={false} type="text" value={this.state.emailValue} onChange={(e) => this.setState({emailValue: e.target.value})}/>
                    <FormControl placeholder="Password" type="password" value={this.state.passwordValue} onChange={(e) => this.setState({passwordValue: e.target.value})}/>

                    <div className="clear floated-links">
                        <Checkbox  checked={this.state.rememberMe} onChange={(e) => this.setState({rememberMe: e.target.checked})} className="float-left no-margin">Remember Me</Checkbox>
                        <Link className="float-right" to={"/forgot?email="+this.state.emailValue} >Forgot Password?</Link>
                    </div>

                    <Button disabled={this.props.loginProcessing} type="submit" bsStyle="primary" style={{width: "100%", marginBottom: 8}}>Login</Button><br/>
                    <span>Don't have an account?&nbsp;<Link to="/sign-up">Sign Up</Link></span>
                </Form>
            );
        }
    }
}

Login.propTypes = {error: PropTypes.object, authenticated: PropTypes.bool, loginProcessing: PropTypes.bool};
Login.defaultProps = {};

const mapStateToProps = (state) => {
    return {
        loginProcessing: state.user.loginProcessing,
        authenticated: state.user.authenticated,
        error: state.user.loginError
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            attemptLogin: (formData) => dispatch(attemptLogin(formData)),
            resendVerification: (email) => dispatch(resendVerificationEmail(email))
        }
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));