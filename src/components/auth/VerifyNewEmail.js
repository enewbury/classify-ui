import React, {Component} from 'react';
import {API, SERVER} from "../../helper/constants";
import {Panel} from "react-bootstrap";
import queryString from "query-string"
import {updateEmail} from "../../actions/userActions";
import {connect} from "react-redux";

class VerifyNewEmail extends Component {

    constructor(props){
        super(props);
        this.state = {
            output: <Panel bsStyle="info"><h3>Verifying your request...</h3></Panel>
        }
    }

    componentDidMount(){
        let token = queryString.parse(this.props.location.search).token;
        let networkErrorOutput = <Panel bsStyle="danger">Looks like we can't connect to the server right now!</Panel>;

        if(!token){
            this.setState({output: <Panel bsStyle="warning">No token parameter to validate.</Panel>})
        } else {
            fetch(SERVER + API + "/user/token", {
                method: "PATCH",
                headers: {'Accept': 'application/json', 'Content-Type': 'application/json'},
                body: JSON.stringify({token, type: "UPDATE_EMAIL"})
            }).then(
                response => {
                    if (response.ok) {
                        this.setState({output: <Panel bsStyle="success"><h3>Verified! Your email has been updated</h3></Panel>});
                        response.json().then(json => this.props.actions.updateEmail(json.email));
                    } else {
                        response.json().then(json => {
                            this.setState({output: <Panel bsStyle="danger">{json.message}</Panel>});
                        });
                    }
                },
                error => {
                    this.setState({output: networkErrorOutput})
                }
            );
        }
    }

    render() {
        return (
            <div style={{margin: "50px auto", textAlign: "center", maxWidth: 400}}>{this.state.output}</div>
        );
    }
}

VerifyNewEmail.propTypes = {};
VerifyNewEmail.defaultProps = {};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            updateEmail: (email) => dispatch(updateEmail(email))
        }
    }
};
export default connect(undefined, mapDispatchToProps)(VerifyNewEmail);