import React, {Component} from 'react';
import {FormControl, Form, Button, Panel, Overlay, Tooltip, FormGroup} from "react-bootstrap"
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import logo from "../layout/Header/logo.png";
import "./authForms.css"
import {Link, Redirect, withRouter} from "react-router-dom";
import {attemptRegistration, resendVerificationEmail} from "../../actions/userActions";
import {SUPPORTED_UNIVERSITIES} from "../../helper/constants";

class SignUp extends Component {

    constructor(props){
        super(props);
        this.state = {
            emailValue: "",
            passwordValue: "",
            confirmValue: "",

            emailValid: true,
            passwordValid: true,
            confirmValid: true,
            validationMessage: "",
            focusedInput: null,
            notice: null
        };
        this.emailInput = null;
        this.passwordInput = null;
        this.confirmInput = null;
        this.attemptRegistration = this.attemptRegistration.bind(this);
        this.resendVerification = this.resendVerification.bind(this);
        this.newFocus = this.newFocus.bind(this);
        this.emailChange = this.emailChange.bind(this);
        this.passwordChange = this.passwordChange.bind(this);
        this.confirmChange = this.confirmChange.bind(this);
    }

    newFocus(component){
        this.setState({focusedInput: component});
    }

    emailChange(e){
        let email = e.target.value;
        this.setState({emailValue: email});
        if(!/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email)){
            this.setState({
                emailValid: false,
                validationMessage: "Not a valid email"
            });
        } else if (!new RegExp(SUPPORTED_UNIVERSITIES.join("|")).test(email.split("@")[1].split(".")[0].toUpperCase())) {
            this.setState({
                emailValid: false,
                validationMessage: "Unsupported university"
            });
        } else {
            this.setState({
                emailValid: true
            });
        }
    }

    passwordChange(e){
        this.setState({passwordValue: e.target.value});
        if(e.target.value.length < 6){
            this.setState({
                passwordValid: false,
                validationMessage: "Password must be at least 6 characters"
            });
        } else {
            this.setState({
                passwordValid: true
            });
        }
    }

    confirmChange(e){
        this.setState({confirmValue: e.target.value});
        if(e.target.value !== this.state.passwordValue){
            this.setState({
                confirmValid: false,
                validationMessage: "Does not match password"
            });
        } else {
            this.setState({
                confirmValid:true
            });
        }
    }

    attemptRegistration(e){
        e.preventDefault();
        this.props.actions.attemptRegistration(this.state.emailValue, this.state.passwordValue);
    }

    resendVerification(){
        this.props.actions.resendVerification(this.state.emailValue);
        this.setState({notice: "Verification email sent to " + this.state.emailValue});
    }

    render() {

        if(this.props.authenticated) {
            const {from} = this.props.location.state || {from: {pathname: '/lists'}};
            return <Redirect to={from}/>
        } else {

            return (
                <div className="full-height-auto">
                <Form className="auth-form" onSubmit={this.attemptRegistration}>
                    <Overlay placement="left" show={(this.state.focusedInput === this.emailInput ) ? !this.state.emailValid : (this.state.focusedInput === this.passwordInput) ? !this.state.passwordValid : !this.state.confirmValid} target={this.state.focusedInput}>
                        <Tooltip id="validation-tooltip">{this.state.validationMessage}</Tooltip>
                    </Overlay>

                    <img style={{width: 40, marginTop: 20}} src={logo} alt="Classify Logo"/>
                    <h2>Let's get started!</h2>
                    {this.props.error && !this.state.notice &&
                        <Panel bsStyle="danger">{this.props.error.message}</Panel>
                    }
                    {this.state.notice &&
                        <Panel bsStyle="info">{this.state.notice}</Panel>
                    }
                    {(this.props.registrationComplete) ?
                        <Panel bsStyle="info">
                            Your account has been created.  Check your email to verify your account.<br />
                            <a onClick={this.resendVerification}>Didn't receive verification email?</a>
                        </Panel>
                    :
                        <div>
                            <FormGroup validationState={(!this.state.emailValid) ? "warning" : null}>
                                <FormControl ref={(input) => this.emailInput = input}
                                             id="email-input"
                                             spellCheck={false}
                                             onFocus={() => this.newFocus(this.emailInput)}
                                             onChange={this.emailChange}
                                             placeholder="University Email"
                                             type="text"
                                />
                            </FormGroup>
                            <FormGroup validationState={(!this.state.passwordValid) ? "warning" : null}>
                                <FormControl ref={(input) => this.passwordInput = input}
                                             id="password-input"
                                             onFocus={() => this.newFocus(this.passwordInput)}
                                             onChange={this.passwordChange}
                                             placeholder="Password"
                                             type="password"
                                />
                            </FormGroup>
                            <FormGroup validationState={(!this.state.confirmValid) ? "warning" : null}>
                                <FormControl id="confirm-input"
                                             ref={(input) => this.confirmInput = input}
                                             onFocus={() => this.newFocus(this.confirmInput)}
                                             onChange={this.confirmChange}
                                             placeholder="Confirm Password"
                                             type="password"
                                />
                            </FormGroup>

                        <Button disabled={this.props.registrationProcessing} type="submit" bsStyle="primary" style={{}}>Sign Up</Button><br/>
                        <span>Already have an account?&nbsp;<Link to="/login">Login</Link></span>
                        </div>
                    }

                </Form>
                </div>
            );
        }
    }
}

SignUp.propTypes = {
    error: PropTypes.object,
    registrationProcessing: PropTypes.bool,
    registrationComplete: PropTypes.bool
};
SignUp.defaultProps = {};

const mapStateToProps = (state) => {
    return {
        registrationProcessing: state.user.registrationProcessing,
        registrationComplete: state.user.registrationComplete,
        error: state.user.registrationError,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: {
            attemptRegistration: (email, password) => dispatch(attemptRegistration(email, password)),
            resendVerification: (email) => dispatch(resendVerificationEmail(email))
        }
    }
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SignUp));