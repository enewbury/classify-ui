import React, {Component} from 'react';
import QueryString from "query-string";
import PropTypes from 'prop-types';
import {Button, Form, FormControl, FormGroup, Panel} from "react-bootstrap";
import {API, JSON_HEADERS, SERVER} from "../../helper/constants";

class Forgot extends Component {
    constructor(props){
        super(props);
        let emailValue = QueryString.parse(props.location.search).email || "";
        this.state = {processing: false, submitted: false, emailValue, error: null};
        this.submit = this.submit.bind(this);
    }

    submit(e){
        e.preventDefault();
        this.setState({processing: true});
        fetch(SERVER + API + "/user/token", {method: "POST", headers: JSON_HEADERS, body:JSON.stringify({email: this.state.emailValue, type: "PASSWORD"})})
            .then(
                response => {
                    if(response.ok){
                        this.setState({processing: false, submitted: true});
                    } else {
                        response.json().then(json => this.setState({processing: false, submitted: true, error: json.message}))
                    }
                },
                error => {
                    this.setState({processing: false, submitted: true, error: "Unable to communicate with server"});
                }
            )
    }

    render() {
        return (
            <Form onSubmit={this.submit} className="auth-form">
                <h3>Forgot your password?</h3>
                {(this.state.submitted) ? (!this.state.error) ?
                        <Panel bsStyle="info">Done! You should receive an email in a few minutes. You may have to check your spam filter.</Panel>
                    :
                        <Panel bsStyle="danger">{this.state.error}</Panel>
                :
                    <div>
                        <p>Enter your email address and we'll send you a link to reset your password.</p>
                        <FormGroup>
                        <FormControl onChange={(e) => this.setState({emailValue: e.target.value})} type="text" value={this.state.emailValue} />
                        </FormGroup>
                        <Button disabled={this.state.processing} type="submit" bsStyle="primary">Submit</Button>
                    </div>
                }

            </Form>
        );
    }
}

Forgot.propTypes = {location: PropTypes.object};
Forgot.defaultProps = {};

export default Forgot;